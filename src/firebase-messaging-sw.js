/*
Give the service worker access to Firebase Messaging.
Note that you can only use Firebase Messaging here, other Firebase libraries are not available in the service worker.
*/

self.addEventListener("notificationclick", function(event) {
    console.log('notification open');
    console.log(event)
// log send to server
});

self.addEventListener("notificationclose", function(event) {
    console.log('notification close');
    // log send to server
  });

importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js')

/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
*/
firebase.initializeApp({
    'messagingSenderId': "868476634905",
    'apiKey': "AIzaSyAf7EgVletnUxvVbQcmVASMl3NF0pQ1yJQ",
    'authDomain': "wia-experience.firebaseapp.com",
    'databaseURL': "https://wia-experience.firebaseio.com",
    'projectId': "wia-experience",
    'storageBucket': "wia-experience.appspot.com",
    'messagingSenderId': "868476634905",
    'appId': "1:868476634905:web:c716564a6a06190204a788"
})

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging()

messaging.setBackgroundMessageHandler(function (payload) {
    const notification = JSON.parse(payload.data.notification);
    // Customize notification here
    const notificationTitle = notification.title;
    const notificationOptions = {
        body: notification.body,
        icon: notification.icon,
        silent: false,
    };

    navigator.vibrate([200, 100, 200,100]);

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});