import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class CheckoutService {
  public cartSubject = new BehaviorSubject(null)
  
  option = environment.apiOpt
  constructor() {}

  async getCartQty(){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/cartqty"+this.option)
    let data = await res.json()
    if(data.message_code != 200) console.warn("Can't get cart qty because "+data.message_dialog)
    else this.cartSubject.next(data)
  }

  async getCartData(){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/cart"+this.option)
    let data = await res.json()
    if(data.message_code != 200) throw("Can't get cart")
    else return data
  }

  async getCheckoutData(){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/quote"+this.option)
    let data = await res.json()
    if(data.message_code != 200) throw("Can't get checkout")
    else return data
  }

  async selectShippingMethod(shippingCode){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/setshipping/code/"+shippingCode+this.option,{
      method: "POST"
    })
    let data = await res.json()
    if(data.message_code != 200) return false
    else return true
  }

  async addToCart(productID: string, option: any){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/addtocart/product/"+productID+this.option,{
      method: "POST",
      body: JSON.stringify(option)
    })
    let data = await res.json()
    if(data.status_code != 200) throw data.message_dialog
    else{
      this.getCartQty()
      return data
    }
  }

  async updateQty(id, newQty){
    let res = await fetch(`${environment.APIBaseURL}/checkoutv3/updatecart/item_id/${id}/qty/${newQty}`+this.option)
    let data = await res.json()
    if(data.message_code == 200){
      this.getCartQty()
      return true
    }
    else return false
  }

  async removeItem(itemID: string){
    let res = await fetch(`${environment.APIBaseURL}/checkoutv3/deleteproduct/item_id/${itemID}`)
    let data = await res.json()
    if(data.message_code == 200){
      this.getCartQty()
      return true
    }
    else return false
  }

  async getVouchers(){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/GetVoucher"+this.option)
    let data = await res.json()
    return data
  }

  async setVoucher(voucherID){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/SetVoucher/id/"+voucherID+this.option)
    let data = await res.json()
    if(data.discount_amount > 200) return true
    else return false
  }

  async createOrder(gift?:{recipient: any,message: any}){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/createorder"+this.option,{
      method: "POST",
      body: JSON.stringify(gift)
    })
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    else {
      this.getCartQty()
      return data
    }
  }
  
  async repayment(order_id){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/pay/order_id/"+order_id)
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    return data
  }

  async reorder(order_id){
    let res = await fetch(environment.APIBaseURL+"/checkoutv3/ReOrder?order_id="+order_id)
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    return data
  }

  async personalize(itemID, optionID, value){
    let res = await fetch(`${environment.APIBaseURL}/Checkoutv3/UpdateCartCustomOptions?
    item_id=${itemID}&option_id=${optionID}&value=${value}`)
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    return data
  }
}
