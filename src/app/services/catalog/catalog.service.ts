import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogService {
  public getCategoryViewURL = environment.APIBaseURL+'/catalog/getcategoryView/id/'
  public searchURL = environment.APIBaseURL+'/catalog/'
  public saleURL=environment.APIBaseURL+'/catalog/getSaleView/id/'

  constructor(
  ) { }

  private linkPreprocessor(APIPath: string, query: string, param?: string){
    query=query.replace(environment.defaultUrl,'')
    query=query.substr(1)
    query=encodeURIComponent(query)

    if(param) param = param.replace('/?','')

    let link
    if(param == '/' || param == null) link = APIPath+query
    else link = APIPath+query+'?'+param

    return link
  }

  async getSaleList(){
    console.log(environment.APIBaseURL+'/catalog/getsales')
    let res = await fetch(environment.APIBaseURL+'/catalog/getsales',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getSaleView(query: string,param){
    let link = this.linkPreprocessor(environment.APIBaseURL+'/catalog/getSaleView/id/',query,param)
    let res = await fetch(link,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getBrandList(){
    let res = await fetch(environment.APIBaseURL+'/catalog/getbrands',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getBrandView(query){
    let link = this.linkPreprocessor(environment.APIBaseURL+'/catalog/getBrandView/id/',query)
    let res = await fetch(link,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCategoryList(){
    let res = await fetch(environment.APIBaseURL+'/catalog/getcategorys',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCategoryView(query: string,param){
    let link = this.linkPreprocessor(environment.APIBaseURL+'/catalog/getcategoryView/id/',query,param)
    let res = await fetch(link,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getProductView(query){
    let link = this.linkPreprocessor(environment.APIBaseURL+'/catalog/getProductView/id/',query)
    let res = await fetch(link,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCampaigns(){
    let res = await fetch(environment.APIBaseURL+'/catalog/getcampaigns',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getCampaignViewByUrlParam(query){
    let link = environment.APIBaseURL+'/catalog/getcampaignview?id=' + query.replace(environment.defaultUrl,'')
    console.log(link)
    let res = await fetch(link,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getFeaturedCategories(){
    let res = await fetch(environment.APIBaseURL+'/catalog/getChildCategoryById/id/659',{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

  async getEditorsPick(){
    let res = await fetch(environment.APIBaseURL+'/catalog/getChildCategoryById/id/198',{
      method: 'GET',
    })
    let data = await res.json()
    return data
  }

  async getFeaturedBrand(query){
    let res=await fetch(environment.APIBaseURL+'/catalog/getCategoryByIds/id/'+query,{
      method: 'GET',
    })
    let data = await res.json()
    return data
  }

  async getFeaturedJournals(){
    let res = await fetch(environment.APIBaseURL+'/catalog/getFeaturedJournal',{
      method: 'GET',
    })
    let data = await res.json()
    return data
  }

  async fetchAny(url: string){
    let res = await fetch(url)
    let data = await res.json()
    return data
  }

  async search(searchQuery: string, page?){
    let link = environment.APIBaseURL+"/catalog/search?q="+searchQuery
    if(page) link+="&p="+page

    let res = await fetch(link)
    let data = await res.json()
    if(data == null) data=[]
    return data
  }

  async getProductCampaign(productID: string){
    let res = await fetch(environment.APIBaseURL+"/Catalog/getChildbyProduct?id="+productID)
    try{
      let data = await res.json()
      return data.campaign == null ? [] : data.campaign
    }catch(e){
      console.warn("No campaign for product "+productID)
      return []
    }
  }

  async getRelatedProducts(productID: string){
    let res = await fetch(environment.APIBaseURL+"/Catalog/getUpSellsProduct?id="+productID)
    let data = await res.json()
    if(data.message_code != 200) throw "Can't get related products because "+data.message_dialog
    else {
      return data.product_list
    }
  }
  
  async getFeaturedProduct(id: string){
    let res = await fetch(environment.APIBaseURL+"/Catalog/getProductbyBrand/brand_id/"+id)
    let data = await res.json()
    return data
  }

  async getFilterCategoryByID(id){
    let res = await fetch(environment.APIBaseURL+"/catalog/getfilterbycategory/id/"+id)
    let data = await res.json()
    return data
  }
  
  async getPromosByProductID(id){
    let res = await fetch(environment.APIBaseURL+"/catalog/getVoucher?product_id="+id)
    let data = await res.json()
    if(data.message_code != 200){
      throw "No promo for this item!"
    }else{
      return data.promo
    }
  }

  async getAllVouchers(){
    let res = await fetch(environment.APIBaseURL+"/catalog/getAllVoucher")
    let data = await res.json()
    if(data.message_code != 200){
      throw "Can't get promo!"
    }else{
      return data.promo
    }
  }

  async getPromosOtherThanVouchers(identifier){
    let res = await fetch(environment.APIBaseURL+"/catalog/getpromo"+"?type="+identifier)
    let data = await res.json()
    return data
  }

  async getSinglePromotions(id){
    let res = await fetch(environment.APIBaseURL+"/promotion?promotion_identifier="+id)
    let data = await res.json()
    if(data.message_code != 200) throw data.message_code
    else return data.promotions
  }

  async getPromoView(query: string){
    let link = this.linkPreprocessor(environment.APIBaseURL+'/catalog/getPromoView/id/',query)
    let res = await fetch(link,{
      method: "GET"
    })
    let data = await res.json()
    return data
  }

}
