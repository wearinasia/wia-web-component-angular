import { Injectable } from '@angular/core';
import { CheckoutService } from '../checkout/checkout.service';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  option = environment.apiOpt
  isLoggedIn = new BehaviorSubject(null)
  
  constructor(
    private checkoutSvc: CheckoutService
  ) {}

  async getOrders(){
    let res = await fetch(environment.APIBaseURL+'/customer/getorders'+this.option)
    let data = await res.json()
    if(data.status_code == 'not_allowed') throw "Can't get orders because You are not allowed"
    else return data.items == null? [] : data.items
  }

  async getOrderDetail(id: string){
    let res = await fetch(environment.APIBaseURL+'/customer/getorderbyid/id/'+id+this.option)
    let data = await res.json()
    if(data.status_code == 'not_allowed') throw "You are not allowed to access this page"
    else return data.item
  }

  async getAddresses(){
    let res = await fetch(environment.APIBaseURL+'/customer/getaddress'+this.option)
    let data = await res.json()
    if(data.status_code == 'not_allowed') throw "You are not allowed to access this page"
    else return data.items
  }

  async getAddress(id: string){
    let res = await fetch(environment.APIBaseURL+'/customer/getAddressById/id/'+id+this.option)
    let data = await res.json()
    if(data.status_code == 'not_allowed') throw "You are not allowed to access this page"
    else return data.item
  }

  async login(loginInfo:{email: string, password: string}){
    let res = await fetch(environment.APIBaseURL+'/customer/login',{
      body: JSON.stringify(loginInfo),
      method: "POST"
    })
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    else{
      this.isLoggedIn.next(true)
      this.checkoutSvc.getCartQty()
      return true
    }
  }

  async logout(){
    let res = await fetch(environment.APIBaseURL+'/customer/logout'+this.option)
    let data = await res.json()

    if(data.message_code != 200) throw data.message_dialog
    else{
      this.isLoggedIn.next(false)
      this.checkoutSvc.getCartQty()
      return true
    }
  }

  async register(loginInfo){
    let res = await fetch(environment.APIBaseURL+'/customer/register',{
      body: JSON.stringify(loginInfo),
      method: "POST"
    })
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    else{
      this.isLoggedIn.next(true)
      return true
    }
  }

  async changePassword(body){
    let res = await fetch(environment.APIBaseURL+'/customer/changepassword',{
      method: "POST",
      body: JSON.stringify(body)
    })
    let data = await res.json()
    if(data.message_code != 200) throw data.message_dialog
    return data
  }

  async getCustomerProfile(){
    let res = await fetch(environment.APIBaseURL+'/customer/getcustomerbyid'+this.option)
    let data = await res.json()

    if(data.status_code == 'not_allowed'){
      this.isLoggedIn.next(false)
      throw "No logged in user"
    }
    else{
      this.isLoggedIn.next(true)
      return data.customer
    }
  }

  async updateProfile(body: any){
    let res = await fetch(environment.APIBaseURL+'/customer/setcustomerbyid/'+this.option,{
      body: JSON.stringify(body),
      method: "PUT"
    })
    let data = await res.json()
    return data
  }

  async setAsShippingAddress(addressID: string){
    let res = await fetch(environment.APIBaseURL+'/checkoutv3/address?id='+addressID+this.option,{
      body: JSON.stringify({}),
      method: "POST"
    })
    let data = await res.json()
    return data
  }

  async updateAddress(id: string, body: any){
    let res = await fetch(environment.APIBaseURL+'/customer/setAddressById/id/'+id+this.option,{
      body: JSON.stringify(body),
      method: "PUT"
    })
    let data = await res.json()
    return data
  }

  async addNewAddress(body: any){
    let res = await fetch(environment.APIBaseURL+'/customer/addAddressById/'+this.option,{
      body: JSON.stringify(body),
      method: "POST"
    })
    let data = await res.json()
    return data
  }

  async getRegions(){
    let res = await fetch(environment.APIBaseURL+'/checkoutv3/regionlist?country=ID')
    let data = await res.json()
    if(data.message_code != 200) throw "Can't get provinces of Indonesia"
    return data.region_list
  }

  async getCitiesOf(regionID: string){
    let res = await fetch(environment.APIBaseURL+'/checkoutv3/citylist?region='+regionID)
    let data = await res.json()
    if(data.message_code != 200) throw "Can't get cities of province "+regionID
    return data.city_list
  }

  async notifyProduct(product_id){
    let res = await fetch(environment.APIBaseURL+'/catalog/PostAlert?entity_id='+product_id,
    {
      method: "POST",
      body: JSON.stringify({})
    })
    let data = await res.json()
    if(data.message_code != 200) throw "Can't notify"
    else return data
  }
}
