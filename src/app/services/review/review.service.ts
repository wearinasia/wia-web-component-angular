import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {
  option = environment.apiOpt

  constructor() {

  }

  async getReviewsByProductID(productID: string){
    let res = await fetch(environment.APIBaseURL+'/review?product='+productID)
    let data = await res.json()
    if(data.message_code != 200) throw "Can't get reviews because "+data.message_dialog
    else{
      return data
    }
  }

  async writeAReview(productID: string, reviewBody: any){
    let res = await fetch(environment.APIBaseURL+'/review?product='+productID,{
      method: "POST",
      body: JSON.stringify(reviewBody)
    })
    let data = await res.json()
    if(data.message_code != 200){
       console.warn("Can't write a review because "+data.message_dialog)
       return false
    }
    return true
  }
}
