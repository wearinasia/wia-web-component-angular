import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterextService {
  
  private previousUrl: string = undefined;
  private currentUrl: string = undefined;

  constructor(private router: Router) {
    this.currentUrl = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
      }
    });
  }

  public getCurrentUrl() {
    return this.currentUrl;
  }

  public getPreviousUrl() {
    if(this.previousUrl == this.currentUrl){
      return '/';
    }

    else{
      return this.previousUrl;
    }
  }

  public hasNoPreviousUrl(){
    return this.previousUrl === this.currentUrl
  }

}
