import { Injectable } from '@angular/core';
import { OrderItem } from 'src/app/model/order';

declare var gtag: any
declare var fbq: any

type GoogleItem={
  id: string
  name: string
  brand?: string
  category?: string
  variant?: string
  quantity?: any
  price?: any
}

@Injectable({
  providedIn: 'root'
})

export class TagService {

  constructor() { }

  addToCart(product: any){
    console.log(parseInt(product.final_price))
    console.log('Google: add_to_cart')
    gtag('event', 'add_to_cart', {
      item: {
        id: product.entity_id,
        name: product.name,
        brand: product.brand,
        price: parseInt(product.final_price),
        quantity: 1,
      }
    })
    
    console.log('FB: add_to_cart')
    fbq('track', 'AddToCart', {
      content_name: product.name, 
      content_ids: [product.entity_id],
      content_type: 'product',
      value: parseInt(product.final_price),
      currency: 'IDR' 
    });         
  }

  purchase(items: OrderItem[]){
    console.log('Google: purchase')
    let total = 0
    let googleItem: GoogleItem[] = []
    items.forEach((item)=>{
      googleItem.push({
        name: item.name,
        id: item.id,
        price: item.price,
        quantity: item.ordered_qty,
      })
      total+=parseInt(item.price)*parseInt(item.ordered_qty)
    })

    gtag('event', 'purchase', {
      item: googleItem
    })

    console.log('FB: purchase')
    fbq('track', 'Purchase', {currency: "IDR", value: total});
    
  }
  
  purchase_unpaid(items: OrderItem[]){
    console.log('Google: purchase_unpaid')
    let total = 0
    let googleItem: GoogleItem[] = []
    items.forEach((item)=>{
      googleItem.push({
        name: item.name,
        id: item.id,
        price: item.price,
        quantity: item.ordered_qty
      })
      total+=parseInt(item.price)*parseInt(item.ordered_qty)
    })

    gtag('event', 'purchase_unpaid', {
      item: googleItem
    })

    console.log('FB: purchase_unpaid')
    fbq('track', 'purchase_unpaid', {currency: "IDR", value: total});

  }

  beginCheckout(quote: any[]){
    console.log('Google: begin_checkout_tag')
    let googleItem: GoogleItem[]=[]
    quote.forEach((item)=>{
      googleItem.push({
        id: item.item_id,
        name: item.name,
        quantity: item.request_quantity,
        price: item.price, 
        brand: item.brand
      })
    })
    gtag('event', 'begin_checkout', {
      items: googleItem
    })

    console.log('FB: begin_fb_checkout')
    let subtotal = 0
    quote.forEach((item)=>{
      subtotal+=item.request_quantity*parseInt(item.price)
    })
    fbq('track', 'InitiateCheckout',{
      value: subtotal+'.00',
      currency: 'IDR',
      contents: quote.map((item) =>{
        return  {
          'id': item.product_id, //product id
          'quantity': item.request_quantity
        }
      }),
      content_type: 'product'
    })

  }

  beginPayment(quote: any){
    console.log('Google: begin_payment')
    let googleItem: GoogleItem[]=[]

    quote.forEach((item)=>{
      googleItem.push({
        id: item.item_id,
        name: item.name,
        quantity: item.request_quantity,
        price: item.price, 
        brand: item.brand
      })
    })
    gtag('event', 'checkout_progress', {
      "items": googleItem,
      "checkout_step": 1,
    });

    console.log('FB: begin_payment_fb')
    let subtotal = 0
    quote.forEach((item)=>{
      subtotal+=item.request_quantity*parseInt(item.price)
    })
    fbq('track', 'PaymentPage',{
      value: subtotal+'.00',
      currency: 'IDR',
      contents: quote.map((item) =>{
        return  {
          'id': item.product_id, //product id
          'quantity': item.request_quantity
        }
      }),
      content_type: 'product'
    })
  }
}
