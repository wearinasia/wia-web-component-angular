import { Injectable } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

type MetaTag={
  name: string,
  content: string,
}

@Injectable({
  providedIn: 'root'
})
export class MetaService {
  private indexingTags=[
    {
      name: "keywords",
      content:"Wearinasia",
    },
    {
      name: "robots",
      content: "INDEX,FOLLOW"
    }
  ]

  private nonIndexingTags=[
    {
      name: "keywords",
      content:"Wearinasia",
    },
    {
      name: "robots",
      content: "NOINDEX,NOFOLLOW"
    }
  ]

  private doNotIndexUrls=[
    'customer'
  ]

  constructor(
    private meta: Meta,
    private title: Title,
    private router: Router
  ) { }

  setTitle(title: string){
    this.title.setTitle(title)
  }

  setTags(metaTags: MetaTag[]){
    
    metaTags.forEach((item)=>{
      this.meta.removeTag(`name='${item.name}'`)
      this.meta.addTag(item)
    })
  }

  checkIndexability(){
    this.indexingTags.forEach((item)=> this.meta.removeTag(`name='${item.name}'`))
    this.nonIndexingTags.forEach((item)=> this.meta.removeTag(`name='${item.name}'`))

    if(this.doNotIndexUrls.every(item => !this.router.url.includes(item))){
      //if address is not forbidden to be indexable
      this.indexingTags.forEach((item)=>{
        this.meta.removeTag(`name='${item.name}'`)
        this.meta.addTag(item)
      })
    }else{
      //if address is forbidden to be indexable
      this.nonIndexingTags.forEach((item)=>{
        this.meta.removeTag(`name='${item.name}'`)
        this.meta.addTag(item)
      })
    }
  }
}
