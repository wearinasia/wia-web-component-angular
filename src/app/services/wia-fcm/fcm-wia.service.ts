import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FcmWiaService {
  private all='all'

  private link = environment.APIBaseURL+'/notification'
  private authorization = "key=AIzaSyDPLB-HSYQmu_PeRqII0Mxwy6DDrlCLWsc"
  
  constructor() {}

  registerFCMToFirebaseTopic(fcm, topic){
    fetch("https://iid.googleapis.com/iid/v1/"+fcm+"/rel/topics/"+topic,{
      method: "POST",
      headers: {
        "Content-Type":"application/json",
        "Authorization": this.authorization
      }
    }).then(
      (res)=>{
        console.log("register topic all success")
      },
    ).catch((err)=>{
      console.log("register topic all fail")
    })
  }

  registerFCMToWia(fcm){
    fetch(this.link,{
      method: "POST",
      body: JSON.stringify({
        "fcm_id": fcm,
      })
    }).then(
      (res)=> res.json()
    ).then(
      (json)=>{
        console.log("register new fcm success!")
      }
    ).catch(
      (err) => {
        if(err.status == 200) console.log("register new fcm success!")
        else console.warn(err)
      }
    )
  }

  updateFCM(newFcm){
    fetch(
      this.link+`?fcm_id=${this.getCookie('wiaFcmToken')}`,{
        method: 'PUT',
        body: JSON.stringify({
          "fcm_id": newFcm,
        })
      }
    ).then(
      (res)=>{
        res.json()
      }
    ).then(
      (json) => {
        console.log("update new fcm success")
      }
    ).catch(
      (err) => console.log(err)
    )
  }

  getCookie(cname){
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return null;
  }
  
}
