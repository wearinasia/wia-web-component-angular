import { Component, OnInit, ElementRef, AfterViewInit, Inject, Renderer2 } from '@angular/core';
import { FcmWiaService } from 'src/app/services/wia-fcm/fcm-wia.service';
import { environment } from 'src/environments/environment';

declare var firebase: any

@Component({
  selector: 'app-fcm',
  templateUrl: './fcm.component.html',
  styleUrls: ['./fcm.component.css']
})
export class FcmComponent implements OnInit {
  messaging: any
  payload: any

  constructor(
    private fcmWiaSvc: FcmWiaService,
    private renderer: Renderer2,
  ){}

  loadFirebaseApp():HTMLScriptElement{
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js"
    this.renderer.appendChild(document.body, script)
    return script;
  }

  loadFirebaseMessaging():HTMLScriptElement{
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js"
    this.renderer.appendChild(document.body, script)
    return script;
  }

  ngOnInit(){
    this.loadFirebaseApp().onload=()=>{
      this.loadFirebaseMessaging().onload=()=>{
        this.initFirebase()
      }
    }
  }

  async initFirebase(){
    if (!firebase.apps.length) {
      await firebase.initializeApp(environment.firebase)
    }
    
    this.messaging = firebase.messaging();

    this.messaging.onMessage(function (payload) {
      //console.log(payload)      
      const notification = payload.notification
      // Customize notification here
      const notificationTitle = notification.title;
      const notificationOptions = {
          body: notification.body,
          icon: notification.icon
      };

    });

    this.messaging.onTokenRefresh(function () {
      this.messaging.getToken().then( async refreshedToken => {
        this.registerTokenLocally(refreshedToken)
        await this.fcmWiaSvc.registerFCMToFirebaseTopic(refreshedToken,'all-goods')
        this.fcmWiaSvc.updateFCM(refreshedToken)
        //?
        }).catch(function (err) {
          console.warn('Unable to retrieve refreshed token ', err);
      });
    });
    
    this.initFirebaseMessagingRegistration()
  }

  registerTokenLocally(token: string){
    console.log("fcm token registering....")
    let expiration = new Date();
    expiration.setHours(expiration.getHours() - 7); // to zulu time
    expiration.setFullYear(expiration.getFullYear()+20); // expire when?
    document.cookie=`wiaFcmToken=${token};expires=${expiration};path=/`
  }

  initFirebaseMessagingRegistration(){
    this.messaging
      .requestPermission()
      .then(()=>{
        this.messaging.getToken().then(async (currentToken)=>{
          await this.fcmWiaSvc.registerFCMToFirebaseTopic(currentToken,'all-goods')
          let fcmToken = this.fcmWiaSvc.getCookie("wiaFcmToken")

          if(fcmToken == null || fcmToken != currentToken){
            this.registerTokenLocally(currentToken)
            this.fcmWiaSvc.registerFCMToWia(currentToken)
          }

        }).catch((err)=>{
          console.warn(err)
        })

      }).catch(function (err) {
        console.warn("FCM still not allowed by you")
    });
  }


}
