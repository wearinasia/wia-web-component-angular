export interface Address{
    entity_id: string;
    customer_id: string;
    firstname: string;
    lastname: string;
    company: string;
    city: string;
    region: string;
    postcode: string;
    country_id: string;
    telephone: string;
    region_id: number;
    street: string;
    location: string;
    vat_id: string;
    is_shipping_address: Boolean;
}