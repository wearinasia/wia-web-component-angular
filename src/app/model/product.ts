export interface StockItem {
  item_id: string;
  product_id: string;
  stock_id: string;
  qty: string;
  min_qty: string;
  use_config_min_qty: string;
  is_qty_decimal: string;
  backorders: string;
  use_config_backorders: string;
  min_sale_qty: string;
  use_config_min_sale_qty: string;
  max_sale_qty: string;
  use_config_max_sale_qty: string;
  is_in_stock: string;
  low_stock_date: string;
  notify_stock_qty: any;
  use_config_notify_stock_qty: string;
  manage_stock: string;
  use_config_manage_stock: string;
  stock_status_changed_auto: string;
  use_config_qty_increments: string;
  qty_increments: string;
  use_config_enable_qty_inc: string;
  enable_qty_increments: string;
  is_decimal_divided: string;
  type_id: string;
  stock_status_changed_automatically: string;
  use_config_enable_qty_increments: string;
  product_name: string;
  store_id: string;
  product_type_id: string;
  product_status_changed: boolean;
  product_changed_websites: any;
}

export interface Image {
  image: string;
  image_2x: string;
  image_3x: string;
}

export interface FeaturedImage {
  image: string;
  image_2x: string;
  image_3x: string;
  image_4x: string;
}

export interface Product {
  entity_id: string;
  url: string;
  url_path: string;
  url_key: string;
  brand: string;
  brand_id: string;
  name: string;
  price: string;
  final_price: any;
  special_price: any;
  stock: any;
  promo: any;
  stock_item: StockItem;
  images: Image[];
  featured_image: FeaturedImage;
  description: string;
  is_in_stock: string;
  brand_url: string;
  installment: any;
  short_description: string;
}