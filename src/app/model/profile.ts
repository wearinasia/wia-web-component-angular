export interface Profile{
    entity_id: string;
    email: string;
    customer_bio: string;
    customer_photo_url: string;
    firstname: string;
    lastname: string;
    default_shipping: string;
    website_id: string;
}