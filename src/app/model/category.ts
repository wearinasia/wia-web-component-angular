import { Product } from './product';

export interface Category{
    entity_id: string,
    name: string,
    type: string,
    label: string,
    title: string
    description: string,
    short_description: string,
    url: string,
    url_key: string,
    url_path: string,
    image: string,
    image_2x: string,
    image_3x: string,
    image_4x: string,
    products: {
        count: any,
        items: Product[]
    }
}

export interface ChildCategory{
    entity_id: string,
    name: string
    url_path: string
}

