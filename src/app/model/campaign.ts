export interface Campaign{
    name: string
    label: string
    title: string
    description: string
    short_description: string
    url: string
    promo: string
    image: string
    image_2x: string
    image_3x: string
    image_4x: string
    url_key: string
    url_path: string
}

