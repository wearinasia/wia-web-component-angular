export interface ShippingAddress {
    entity_id: string;
    parent_id: string;
    customer_address_id: string;
    quote_address_id?: any;
    region_id: string;
    customer_id: string;
    fax?: any;
    region: string;
    postcode: string;
    lastname: string;
    street: string;
    city: string;
    email: string;
    telephone: string;
    country_id: string;
    firstname: string;
    address_type: string;
    prefix?: any;
    middlename?: any;
    suffix?: any;
    company: string;
    vat_id?: any;
    vat_is_valid?: any;
    vat_request_id?: any;
    vat_request_date?: any;
    vat_request_success?: any;
    delivery_instruction?: any;
}


export interface OrderItem{
    name: string;
    sku: string;
    id: string;
    price: string;
    ordered_qty: string;
    image?: any;
}

export interface Order {
    status_code: string;
    entity_id: string;
    increment_id: string;
    status: string;
    status_label    : string;
    state: string;
    order_date: Date;
    customer_id: string;
    grand_total: string;
    shipping_method: string;
    payment_method: string;
    shipping_address: ShippingAddress;
    items: OrderItem[]
}
