import { Product } from './product';

export interface Brand{
    name: string
    label: string
    title: string
    description: string
    short_description: string
    url: string
    promo: string
    image: string
    image_2x: string
    image_3x: string
    image_4x: string
    url_key: string
    url_path: string
    featured_image: any
    products: {
        count: any,
        items: any[]
    }
}