import { Product } from './product';

export interface Sale{
    name: string
    label: any
    title: string
    description: string
    short_description: string
    url: string
    url_key: string
    url_path: string
    promo: string
    image: string
    image_2x: string
    image_3x: string
    image_4x: string
    products: {
        count: any,
        items: any[]
    }
}