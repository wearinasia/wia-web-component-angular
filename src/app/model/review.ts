export interface Reviews{
  "summary": any,
  "count": any,
  "percent": any,
  "reviews": Review[]
}

export interface Review {
    "id": string,
    "product_id": string,
    "status_id": string,
    "title": string,
    "detail": string,
    "created": any,
    "customer": {
      "customer_id": string,
      "name": string,
      "photo": any,
      "bio": string
    },
    "value": any,
    "percent": any,
    "count": any
}