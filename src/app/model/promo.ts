export interface Promo{
    "name": string
    "description": string
    "code": string
    "from_date": any,
    "to_date": any
    "simple_action": string
    "discount_amount": string
    url_key: string
    url_path: string
    image?: any
}