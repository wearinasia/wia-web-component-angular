import { Component, OnInit } from '@angular/core';
import { faReceipt ,faPercent,faThLarge,faUserCircle,faHome,faTags, faFileInvoiceDollar } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-bottom-navigation-bar',
  templateUrl: './bottom-navigation-bar.component.html',
  styleUrls: ['./bottom-navigation-bar.component.css']
})
export class BottomNavigationBarComponent implements OnInit {
  faUserCircle = faUserCircle
  faHome = faHome
  faFileInvoiceDollar = faFileInvoiceDollar
  faTags= faTags
  faReceipt = faReceipt
  faThLarge= faThLarge
  faPercent =faPercent

  isScrollingDown: boolean
  constructor() { }

  ngOnInit() {
    var scrollableElement = document.body;
    var clientX, clientY;

    scrollableElement.addEventListener('wheel', (event: any)=>{
      if (event.deltaY) {
        if(event.deltaY>0) this.isScrollingDown=true
        else this.isScrollingDown=false
      }
    });

    scrollableElement.addEventListener('touchstart', (e)=>{
      // Cache the client X/Y coordinates
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    }, false);
    
    scrollableElement.addEventListener('touchend', (e)=>{
      var deltaX, deltaY;
    
      // Compute the change in X and Y coordinates. 
      // The first touch point in the changedTouches
      // list is the touch point that was just removed from the surface.
      deltaX = e.changedTouches[0].clientX - clientX;
      deltaY = e.changedTouches[0].clientY - clientY;
  
      deltaY < 0 ? this.isScrollingDown=true : this.isScrollingDown=false
    },false);

  }
  

}
