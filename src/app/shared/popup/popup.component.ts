import { Component, OnInit, Input, OnDestroy, Renderer2, RendererFactory2, Output, EventEmitter} from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css']
})
export class PopupComponent implements OnInit, OnDestroy {
  @Input() isHalf: boolean
  
  class:{}

  @Input() isOpen: Boolean
  @Input() name: any
  @Input() title?

  faTimes = faTimes

  @Output() onClose = new EventEmitter()

  private renderer: Renderer2;
  
  constructor(
    private rendererFactory: RendererFactory2,
  ) {
    this.renderer = this.rendererFactory.createRenderer(null, null);
  }

  ngOnInit() {
    this.class={
      'slideup-container': true,
      'half' : this.isHalf,
      'show' : true,
    }

    this.renderer.setAttribute(document.body,'class', 'no-scroll');
  }

  ngOnDestroy(){
    this.renderer.removeAttribute(document.body, 'class')
  }

  close(){
    this.onClose.emit(this.name)
  }

}
