import { Component, OnInit } from '@angular/core';
import { Campaign } from 'src/app/model/campaign';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-featured-campaign',
  templateUrl: './featured-campaign.component.html',
  styleUrls: ['./featured-campaign.component.css']
})
export class FeaturedCampaignComponent implements OnInit {
  items : []
  isLoading: boolean


  constructor(
    private catalogSvc: CatalogService,
  ) { }

  ngOnInit() {
    this.getCampaigns()
  }

  async getCampaigns(){
    try{
      this.isLoading=true
      this.items = await this.catalogSvc.getCampaigns()
      //console.log(this.items)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

}
