import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedCampaignComponent } from './featured-campaign.component';

describe('FeaturedCampaignComponent', () => {
  let component: FeaturedCampaignComponent;
  let fixture: ComponentFixture<FeaturedCampaignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedCampaignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
