import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-featured-categories',
  templateUrl: './featured-categories.component.html',
  styleUrls: ['./featured-categories.component.css']
})
export class FeaturedCategoriesComponent implements OnInit {
  categories: any
  isLoading: boolean;
  constructor(
    private catalogSvc: CatalogService
  ) { }

  async ngOnInit() {
    await this.getfeaturedcategories()
  }

  async getfeaturedcategories(){
    try{
      this.isLoading=true
      this.categories = await this.catalogSvc.getCategoryList()
    }catch(e){
    
    }finally{
      this.isLoading=false
    }
  }

  isSelected(index){
    return{
      'activated': location.pathname.includes(this.categories[index].url_path),
      'not-activated' : !location.pathname.includes(this.categories[index].url_path),
    }
  }

}
