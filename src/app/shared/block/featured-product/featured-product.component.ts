import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-featured-product',
  templateUrl: './featured-product.component.html',
  styleUrls: ['./featured-product.component.css']
})
export class FeaturedProductComponent implements OnInit {
  isLoading: boolean
  @Input() id: string
  featuredProducts: any

  constructor(
    private catalogSvc: CatalogService
  ) { }

  ngOnInit() {
    this.getFeaturedProducts()
  }

  async getFeaturedProducts(){
    try{
      this.isLoading=true
      this.featuredProducts = await this.catalogSvc.getFeaturedProduct(this.id)
      //console.log(this.featuredProducts)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }
}
