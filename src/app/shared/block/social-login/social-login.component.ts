import { Component, OnInit, Output, EventEmitter, Renderer2 } from '@angular/core';
import { FbSessionService, FBLoginState } from 'src/app/services/fb-session/fb-session.service';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css']
})
export class SocialLoginComponent implements OnInit {
  disableFBButton: boolean
  @Output() onSuccessFacebookLogin = new EventEmitter()
  
  constructor(
    private fbSesSvc: FbSessionService,
    private toastSvc: ToastService,
    private renderer: Renderer2

  ) { }

  addJsToElement(): HTMLScriptElement {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v5.0&appId=1792367184373817&autoLogAppEvents=1"
    script.crossOrigin="anonymous"
    this.renderer.appendChild(document.body, script)
    return script;
  }

  ngOnInit() {
    this.disableFBButton=true
    this.addJsToElement().onload=()=>{
      this.disableFBButton=false
    }
  }

  async loginFB(){
    this.fbSesSvc.fbTokens.subscribe(
      async (t)=>{
        if(t){
          let res = await this.fbSesSvc.signInUsingFBToken({
            token: t.authResponse.accessToken
          })
          if(res){
            this.toastSvc.sendMessage("Login with facebook was successfull!")
            this.onSuccessFacebookLogin.emit(true)
          }else{
            this.onSuccessFacebookLogin.emit(false)
          }
        }
      }
    )
    
    this.fbSesSvc.loginByFacebook()
  }

}
