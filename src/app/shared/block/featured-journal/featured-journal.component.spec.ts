import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedJournalComponent } from './featured-journal.component';

describe('FeaturedJournalComponent', () => {
  let component: FeaturedJournalComponent;
  let fixture: ComponentFixture<FeaturedJournalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedJournalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedJournalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
