import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'featured-journal',
  templateUrl: './featured-journal.component.html',
  styleUrls: ['./featured-journal.component.css']
})
export class FeaturedJournalComponent implements OnInit {
  journal: any
  constructor(
    public catalogSvc: CatalogService
  ) { }

  async ngOnInit() {
    try{
      this.journal = await this.catalogSvc.getFeaturedJournals()
    }catch(e){
      console.warn(e)
    }
  }

}
