import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturedSaleComponent } from './featured-sale.component';

describe('FeaturedSaleComponent', () => {
  let component: FeaturedSaleComponent;
  let fixture: ComponentFixture<FeaturedSaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeaturedSaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeaturedSaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
