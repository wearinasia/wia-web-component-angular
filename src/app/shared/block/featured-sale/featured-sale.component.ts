import { Component, OnInit } from '@angular/core';
import { Sale } from 'src/app/model/sale';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-featured-sale',
  templateUrl: './featured-sale.component.html',
  styleUrls: ['./featured-sale.component.css']
})
export class FeaturedSaleComponent implements OnInit {
  sales: []
  isLoading: Boolean
  constructor(
    private catalogSvc: CatalogService
  ) { }

  ngOnInit() {
    this.isLoading=true
    this.getSale()
  }

  async getSale(){
   
    try{
      this.sales = await this.catalogSvc.getSaleList()
      //console.log(this.sales)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

}
