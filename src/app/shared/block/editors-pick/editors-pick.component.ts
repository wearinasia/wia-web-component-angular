import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-editors-pick',
  templateUrl: './editors-pick.component.html',
  styleUrls: ['./editors-pick.component.css']
})
export class EditorsPickComponent implements OnInit {
  picks: any
  isLoading: boolean;
  
  constructor(
    private catalogSvc: CatalogService
  ) { }

  async ngOnInit() {
    this.isLoading=true 
    try{
      this.picks = await this.catalogSvc.getEditorsPick()
    }catch(e){
      console.warn(e)
    }
    finally{
      this.isLoading=false
    }
  }

}
