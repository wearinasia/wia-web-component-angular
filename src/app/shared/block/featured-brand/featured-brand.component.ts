import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-featured-brand',
  templateUrl: './featured-brand.component.html',
  styleUrls: ['./featured-brand.component.css']
})

export class FeaturedBrandComponent implements OnInit{
  
  @Input() ids: string
  @Input() scroll?: boolean
  brands: any
  isLoading: Boolean

  constructor(
    private catalogSvc: CatalogService
  ) { }


  async ngOnInit() {
    this.isLoading = true
    try{
      this.brands = await this.catalogSvc.getFeaturedBrand(this.ids)
      console.log(this.brands)
      this.brands.forEach((item)=>{
        if(item.image_2x == false) item.image_2x = "/assets/placeholders/wia.jpg"
      })
    }catch(e){
      console.warn(e)
    }
    finally{
      this.isLoading=false
    }
  }

}
