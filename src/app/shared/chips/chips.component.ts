import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ChildCategory } from 'src/app/model/category';


@Component({
  selector: 'app-chips',
  templateUrl: './chips.component.html',
  styleUrls: ['./chips.component.css']
})
export class ChipsComponent implements OnInit {
  @Input('list') childCategoryList?: ChildCategory[]
  @Output() id : EventEmitter<string> = new EventEmitter()

  constructor() {}

  ngOnInit() {
  }

  emit(id){
    this.id.emit(this.childCategoryList[id].url_path)
  }

  active(i){
    return {
      'selected': location.pathname.includes(this.childCategoryList[i].url_path)
    }
  }
}
