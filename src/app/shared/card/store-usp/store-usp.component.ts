import { Component, OnInit } from '@angular/core';

import { faChevronCircleDown,faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-store-usp',
  templateUrl: './store-usp.component.html',
  styleUrls: ['./store-usp.component.css']
})
export class StoreUspComponent implements OnInit {
  faChevronCircleDown=faChevronCircleDown
  faChevronRight=faChevronRight
  constructor() { }

  ngOnInit() {
  }

}
