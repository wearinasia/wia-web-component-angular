import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoreUspComponent } from './store-usp.component';

describe('StoreUspComponent', () => {
  let component: StoreUspComponent;
  let fixture: ComponentFixture<StoreUspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoreUspComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoreUspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
