import { Component, OnInit } from '@angular/core';
import { faPhone,faCommentAlt } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-customer-service',
  templateUrl: './customer-service.component.html',
  styleUrls: ['./customer-service.component.css']
})
export class CustomerServiceComponent implements OnInit {
  faCommentAlt=faCommentAlt
  faPhone=faPhone
  constructor() { }

  ngOnInit() {
  }
  showDeskhelp(){
    eval("window.fcWidget.open();window.fcWidget.show()")
  }

}
