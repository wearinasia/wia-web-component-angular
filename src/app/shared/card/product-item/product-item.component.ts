import { Component, OnInit, Input, OnChanges, AfterViewChecked, AfterViewInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { faTags } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.css']
})
export class ProductItemComponent implements OnInit, AfterViewChecked{

  @Input() item: Product
  faTags = faTags

  constructor() {
  }

  ngAfterViewChecked(){
    if(!this.item) return
    let img=document.getElementById("i"+this.item.entity_id)
    img.setAttribute('src','../assets/placeholders/loading.gif')
    img.setAttribute('data-srcset',this.makeDatasrcset())
  }

  ngOnInit() {
   
  }
  
  getItemDiscountPercentage(){
    let diff = Math.abs(parseFloat(this.item.final_price) - parseFloat(this.item.price))
    if(diff > 0){
      let frac = diff/parseFloat(this.item.price)
      return frac*100
    }else{
      return null
    }
  }

  getInstallmentTariff(){
    return this.item.final_price / this.item.installment
  }

  isProductDiscounted(){
    let finalPrice = this.item.final_price
    let price = this.item.price
    return finalPrice != price
  }

  notInStock(){
    return this.item.is_in_stock == '0'
  }

  makeSrc(){
    return this.item.featured_image.image_2x
  }

  makeDatasrcset(){
    return `
    ${this.item.featured_image.image_2x} 2x, 
    ${this.item.featured_image.image} 1x
    `
  }
}
