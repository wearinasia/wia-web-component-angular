import { Component, OnInit } from '@angular/core';
import { faChevronRight ,faCommentAlt,faPhone,faMap } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.css']
})
export class StoreComponent implements OnInit {

  constructor() { }
  faCommentAlt=faCommentAlt
  faPhone= faPhone
  faMap=faMap
  faChevronRight=faChevronRight
  ngOnInit() {
  }
  showDeskhelp(){
    eval("window.fcWidget.open();window.fcWidget.show()")
  }

}
