import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { faClock,faPercentage } from '@fortawesome/free-solid-svg-icons';
import { Promo } from 'src/app/model/promo';
import { ToastService } from 'src/app/services/global/toast.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-promo-item',
  templateUrl: './promo-item.component.html',
  styleUrls: ['./promo-item.component.css']
})
export class PromoItemComponent implements OnInit, OnDestroy{
  ngOnDestroy(){
    if(this.timeUpdate) clearInterval(this.timeUpdate)
  }

  @Input() promo?: Promo
  stopwatchIcon=faClock
  percentageIcon=faPercentage

  private coeffTime: any
  days: any
  hours: any
  mins: any
  seconds: any

  timeUpdate: any
  
  constructor(
    private toastSvc: ToastService,
    private router: Router
  ) { }

  ngOnInit() {
    if(this.promo){
      try{
        if(this.promo.url_path.includes('sale-1') || this.promo.url_path.includes('sale')){
          this.promo.url_path = 'sale/'+this.promo.url_key
        }
      }catch(e){
        
      }
      finally{
        this.calculateTimeLeft()
        this.timeUpdate = setInterval(()=>{
          this.calculateTimeLeft()
        },1000)
      }
    }
  }

  calculateTimeLeft(){
    this.coeffTime = Math.floor( (new Date(this.promo.to_date).getTime() - new Date().getTime()) / 1000)
    this.days=Math.floor(this.coeffTime/86400)
    this.hours=Math.floor( (this.coeffTime%86400) / 3600 )
    this.mins=Math.floor( (this.coeffTime%86400%3600) / 60)
    this.seconds=Math.floor( (this.coeffTime%86400%3600%60) )

    if(this.promoExpired()) clearTimeout(this.timeUpdate)
  }

  copyCode(){

    let selBox = document.createElement('textarea');

    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.promo.code;

    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();

    document.execCommand('copy');
    document.body.removeChild(selBox);

    this.toastSvc.sendMessage(`${this.promo.code} has been copied to clipboard`)
  }

  navigate(){
    if(this.promo.url_path) this.router.navigate(['/'+this.promo.url_path])
  }

  promoExpirationNotSpecified(){
    return !this.promo.to_date
  }

  promoExpired(){
    return this.coeffTime < 0
  }

  generateTimeLeft(){
    try{
      if(this.days > 0) return `Berakhir dalam ${this.days} hari lagi`
      else return `Berakhir dalam ${this.hours.toString().padStart(2,'0')}:${this.mins.toString().padStart(2,'0')}:${this.seconds.toString().padStart(2,'0')}`
    }catch(e){
      return ""
    }
  }

  hasPromoCode(){
    return this.promo.code != null
  }

}
