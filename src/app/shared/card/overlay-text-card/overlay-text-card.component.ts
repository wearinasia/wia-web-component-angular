import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-overlay-text-card',
  templateUrl: './overlay-text-card.component.html',
  styleUrls: ['./overlay-text-card.component.css']
})
export class OverlayTextCardComponent implements OnInit {
  data: any
  isLoading: Boolean

  constructor(
    private catalogSvc: CatalogService
  ) { }

  async ngOnInit() {
    try{
      this.isLoading=true
      let res = await this.catalogSvc.getSinglePromotions('homepage_banner')
      console.log(res)
      this.data=res
    }catch(e){

    }finally{
      this.isLoading=false
    }
  }

}
