import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverlayTextCardComponent } from './overlay-text-card.component';

describe('OverlayTextCardComponent', () => {
  let component: OverlayTextCardComponent;
  let fixture: ComponentFixture<OverlayTextCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverlayTextCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverlayTextCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
