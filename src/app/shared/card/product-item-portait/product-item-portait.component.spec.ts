import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductItemPortaitComponent } from './product-item-portait.component';

describe('ProductItemPortaitComponent', () => {
  let component: ProductItemPortaitComponent;
  let fixture: ComponentFixture<ProductItemPortaitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductItemPortaitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductItemPortaitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
