import { Component, OnInit, Input, AfterViewChecked } from '@angular/core';
import {faTags} from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-product-item-portait',
  templateUrl: './product-item-portait.component.html',
  styleUrls: ['./product-item-portait.component.css']
})
export class ProductItemPortaitComponent implements OnInit, AfterViewChecked {
  faTag = faTags
  @Input() item: any
  
  constructor() { }

  ngOnInit() {
  }

  ngAfterViewChecked(){
    if(!this.item) return

    let img=document.getElementById("i"+this.item.entity_id)
    img.setAttribute('src',this.makeSrc())
    img.setAttribute('data-srcset',this.makeDatasrcset())
  }

  makeSrc(){
    return this.item.featured_image.image_2x
  }

  makeDatasrcset(){
    let text: string = `
    ${this.item.featured_image.image_2x} 2x, 
    ${this.item.featured_image.image} 1x
    `
    return text
  }

  isProductDiscounted(){
    let finalPrice = this.item.finalPrice
    let price = this.item.price
    return price != finalPrice
  }

}
