import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons';
import { UrlSerializer, Router, ActivatedRoute } from '@angular/router';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-lazy-loading',
  templateUrl: './lazy-loading.component.html',
  styleUrls: ['./lazy-loading.component.css']
})
export class LazyLoadingComponent implements OnInit, OnChanges{
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.hasOwnProperty('additionalParam')){
      this.currentPage=1
      this.isMaximumOfList=false
    }
  }

  @Input() resourceURL: string
  @Input() additionalParam?: {}

  @Output() onSuccessRetrieve = new EventEmitter()

  faLoad = faCircleNotch
  currentPage: any
  isLazyLoading: Boolean
  isMaximumOfList: Boolean

  observer: IntersectionObserver
  
  private query
  private param

  constructor(
    private router: Router,
    private serializer: UrlSerializer,
    private catalogSvc: CatalogService,
  ) {
    this.query=location.pathname
    this.isLazyLoading = false
    this.isMaximumOfList = false
  }

  ngOnInit(){
    this.currentPage = 1
    let threshold = [0.25,0.50,0.75,1.00]
    let triggerEl = document.getElementById("scrollArea")

    //do not create lazy loading if the trigger element is not exist
    if(triggerEl == null) return
    
    let options = {
      threshold: threshold,
      rootMargin: '0px 0px 2px 0px',
    }

    this.observer = new IntersectionObserver(entries => {
      entries.forEach(async entry => {
        console.log(this.currentPage)
        if (entry.intersectionRatio >= 0.75 && !this.isLazyLoading && !this.isMaximumOfList) {
          try{
            this.currentPage+=1

            let paramTemp=this.additionalParam
            if(paramTemp == null) paramTemp = {p: this.currentPage}
            else{
              paramTemp['p']=this.currentPage
            }

            this.param=this.serializer.serialize(this.router.createUrlTree([''], { queryParams: paramTemp}))

            this.isLazyLoading = true
            
            let link = this.linkPreprocessor(this.resourceURL,this.query,this.param)
            let res = await this.catalogSvc.fetchAny(link)
            
            ///concating list of products in the current categories
            if(res.products.count == 0){
              this.isMaximumOfList=true
              this.onSuccessRetrieve.emit(false)
            }else{
              this.onSuccessRetrieve.emit(res)
            }

          }catch(e){
            console.warn(e)
          }finally{
            this.isLazyLoading =false
          }

        }
      });
    },options);
    
    this.observer.observe(triggerEl);
  }

  private linkPreprocessor(APIPath: string, query: string, param?: string){
    query=query.replace(environment.defaultUrl,'')
    query=query.substr(1)
    query=encodeURIComponent(query)

    let link
    param = param.replace('/?','')
    if(param == null) link = APIPath+query
    else {
      link = APIPath+query+'?'+param
    }
    return link
  }

}
