import { Component, OnInit, Input, Output,EventEmitter, OnChanges } from '@angular/core';
import { ActivatedRoute, UrlSerializer, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-filter-by',
  templateUrl: './filter-by.component.html',
  styleUrls: ['./filter-by.component.css']
})
export class FilterByComponent implements OnInit, OnChanges {
  shown: Boolean

  @Input() filters: any
  @Output() filterChanges = new EventEmitter()
  filtersSelection: any
  filtersSelectionInQueryParams: any

  constructor(
    private activatedRoute: ActivatedRoute,
    private serializer: UrlSerializer,
    private router: Router
  ) { }

  ngOnChanges(): void {
    this.ngOnInit()
  }

  ngOnInit() {
     //extract aspects
     this.filtersSelection={}
     if(this.filters){
       this.filters.filter.forEach((i)=>{
         this.filtersSelection[i.code] = {}
       })
       this.destructureFilterCSV()
     } 
  }

  show(){
    this.shown=true
  }

  closeModal(){
    this.shown=false
  }

  destructureFilterCSV(){
    let input = this.activatedRoute.snapshot.queryParams
    //empty the filterSelection keys
    for(let aspect in this.filtersSelection){
      this.filtersSelection[aspect]={}
    }

    if(Object.keys(input).length == 0) return

    for(let aspect in input){
      let strings=input[aspect].split(',')
      strings.forEach((item)=>{
        this.filtersSelection[aspect][item]=true
      })
    }
    
    this.filtersSelectionInQueryParams=this.createFilterCSV()
    this.filterChanges.emit(this.filtersSelectionInQueryParams)

  }

  /*create filters into CSV queryParams format
  from
  {
    a:{
      '11':true,
      '22':true,
      '33':true
    }
  } 

  to

  {a:'11,22,33'} 

  */
  private createFilterCSV(){
    let res = {}
    for(let aspect in this.filtersSelection){
    let strings=[]
    for(let val in this.filtersSelection[aspect]) strings.push(val)
    if(strings.length != 0) res[aspect]=strings.join(',')
    }
    return res
  }

  updateCurrFilters(){
     //this function to clears any key in the aspect that value false
    //So if the customer select or unselect the filters, it must be true or gone
    for(let aspect in this.filtersSelection){
      for(let val in this.filtersSelection[aspect]){
        if(!this.filtersSelection[aspect][val]) delete this.filtersSelection[aspect][val]
      }
    }
  }

  toggleChip(aspect,item){
    this.filtersSelection[aspect][item] = !this.filtersSelection[aspect][item]
    this.updateCurrFilters()
  }

  applyFilter(){
    //let's apply the this.filtersSelection to the URL
    this.filtersSelectionInQueryParams=this.createFilterCSV()
    this.filterChanges.emit(this.filtersSelectionInQueryParams)
    let link = this.serializer.serialize(this.router.createUrlTree([location.pathname.replace(environment.defaultUrl,'')],{ queryParams: this.createFilterCSV()}))
    this.closeModal()
    this.router.navigateByUrl(link)
  
  }


}
