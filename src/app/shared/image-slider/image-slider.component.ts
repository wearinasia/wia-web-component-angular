import { Component, OnInit, Input, OnChanges, AfterViewInit, AfterViewChecked } from '@angular/core';
import Glide from '@glidejs/glide'
import { Image } from 'src/app/model/product'
declare var yall: any

@Component({
  selector: 'app-image-slider',
  templateUrl: './image-slider.component.html',
  styleUrls: ['./image-slider.component.css']
})

export class ImageSliderComponent implements AfterViewInit{
  @Input('images') images: Image[]
  glide: Glide

  ngAfterViewInit() {
    var slider = document.getElementById('productSlide')
    
    if(this.images == null) return
    for(let item of this.images){
      let wrapper = document.createElement('div')
      wrapper.className="glide__slide"

      let container = document.createElement('div')
      container.className="product-image padding-center text-center"
      
      let img = document.createElement('img')
      img.height=600
      img.width=800
      img.src=item.image_2x

      container.appendChild(img)

      wrapper.appendChild(container)
      slider.appendChild(wrapper)
    }
    this.glide = new Glide('.glide').mount()

  }

  makeDatasrc(item: Image){
    return item.image
  }

  makeDatasrcset(item: Image){
    return `
    ${item.image_3x} 3x, 
    ${item.image_2x} 2x, 
    ${item.image} 1x
    `
  }

}
