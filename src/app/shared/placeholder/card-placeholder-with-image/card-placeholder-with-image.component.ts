import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card-placeholder-with-image',
  templateUrl: './card-placeholder-with-image.component.html',
  styleUrls: ['./card-placeholder-with-image.component.css']
})
export class CardPlaceholderWithImageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
