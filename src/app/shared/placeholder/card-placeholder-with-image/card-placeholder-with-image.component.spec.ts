import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CardPlaceholderWithImageComponent } from './card-placeholder-with-image.component';

describe('CardPlaceholderWithImageComponent', () => {
  let component: CardPlaceholderWithImageComponent;
  let fixture: ComponentFixture<CardPlaceholderWithImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CardPlaceholderWithImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CardPlaceholderWithImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
