import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTileWithImageComponent } from './list-tile-with-image.component';

describe('ListTileWithImageComponent', () => {
  let component: ListTileWithImageComponent;
  let fixture: ComponentFixture<ListTileWithImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTileWithImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTileWithImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
