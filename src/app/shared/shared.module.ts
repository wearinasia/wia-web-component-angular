import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopNavigationBarComponent } from './top-navigation-bar/top-navigation-bar.component';
import { RouterModule } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BottomNavigationBarComponent } from './bottom-navigation-bar/bottom-navigation-bar.component';
import { LoadingComponent } from './loading/loading.component';
import { PopupComponent } from './popup/popup.component';
import { ToastComponent } from './toast/toast.component';
import { CustomerServiceComponent } from './card/customer-service/customer-service.component';
import { LazyLoadingComponent } from './container/lazy-loading/lazy-loading.component';
import { GlobalFooterComponent } from './global-footer/global-footer.component';
import { StoreComponent } from './card/store/store.component';
import { PromoItemComponent } from './card/promo-item/promo-item.component';
import { ReviewItemComponent } from './card/review-item/review-item.component';
import { ProductItemPortaitComponent } from './card/product-item-portait/product-item-portait.component';
import { ImageSliderComponent } from './image-slider/image-slider.component';
import { ChipsComponent } from './chips/chips.component';
import { ProductItemComponent } from './card/product-item/product-item.component';
import { StoreUspComponent } from './card/store-usp/store-usp.component';
import { EditorsPickComponent } from './block/editors-pick/editors-pick.component';
import { SocialLoginComponent } from './block/social-login/social-login.component';
import { ListTileWithImageComponent } from './placeholder/list-tile-with-image/list-tile-with-image.component';
import { ListTileComponent } from './placeholder/list-tile/list-tile.component';
import { CardPlaceholderWithImageComponent } from './placeholder/card-placeholder-with-image/card-placeholder-with-image.component';
import { FilterByComponent } from './container/filter-by/filter-by.component';
import { CampaignItemComponent } from './card/campaign-item/campaign-item.component';
import { OverlayTextCardComponent } from './card/overlay-text-card/overlay-text-card.component';

const core=[
    CommonModule,
    RouterModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
]

const frequentlyUsedComponent=[
    TopNavigationBarComponent,
    BottomNavigationBarComponent,
    GlobalFooterComponent,
    StoreComponent,
    PopupComponent,
    LoadingComponent,
    ToastComponent,
    CustomerServiceComponent,
    LazyLoadingComponent,
    PromoItemComponent,
    ReviewItemComponent,
    ProductItemPortaitComponent,
    ImageSliderComponent,
    ChipsComponent,
    ProductItemComponent,
    StoreUspComponent,
    EditorsPickComponent,
    SocialLoginComponent,
    ListTileWithImageComponent,
    ListTileComponent,
    CardPlaceholderWithImageComponent,
    FilterByComponent,
    CampaignItemComponent,
    OverlayTextCardComponent
]

@NgModule({
    imports: core,
    declarations:frequentlyUsedComponent,
    exports:[
        ...frequentlyUsedComponent,
        ...core,
    ]
})
export class SharedModule { }