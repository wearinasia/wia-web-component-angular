import { Component, OnInit } from '@angular/core';
let deferredPrompt: any

@Component({
  selector: 'app-global-footer',
  templateUrl: './global-footer.component.html',
  styleUrls: ['./global-footer.component.css']
})
export class GlobalFooterComponent implements OnInit {
  notInstalled: Boolean

  constructor() { }

  ngOnInit() {
    window.addEventListener('beforeinstallprompt', (e) => {
      console.log(e)
      // Prevent the mini-infobar from appearing on mobile
      e.preventDefault();
      // Stash the event so it can be triggered later.
      deferredPrompt = e;
      // Update UI notify the user they can install the PWA
      this.notInstalled=true
    });

    window.addEventListener('appinstalled', (evt) => {
      console.log('a2hs installed');
      this.notInstalled=false
    });

  }
  

  addToHomeScreen() {
    this.notInstalled=false
    deferredPrompt.prompt();
  
    deferredPrompt.userChoice.then(choiceResult =>{

      if (choiceResult.outcome === 'accepted') {
        console.info('mm User accepted the A2HS prompt');
      } else {
        console.info('mm User dismissed the A2HS prompt');
      }
  
      deferredPrompt = null;
    })
  }
}
