import { Component, OnInit, Input, OnChanges, NgZone } from '@angular/core';
import { Location } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { RouterextService } from 'src/app/services/routerext/routerext.service';
import { faTag ,faArrowLeft,faSearch,faCommentDots,faShoppingBag,faUserCircle, faInfoCircle } from '@fortawesome/free-solid-svg-icons';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';

@Component({
  selector: 'app-top-navigation-bar',
  templateUrl: './top-navigation-bar.component.html',
  styleUrls: ['./top-navigation-bar.component.css']
})
export class TopNavigationBarComponent implements OnInit {
  faArrowLeft = faArrowLeft;
  faSearch = faSearch;
  faCommentDots = faCommentDots;
  faShoppingBag = faShoppingBag;
  faUserCircle=faUserCircle;
  faHelp = faInfoCircle
  faTags=faTag

  @Input() transparant: boolean 
  @Input() title = null || '' ;
  @Input() routerLink?: string
  @Input() noBackButton?: boolean

  @Input() hideMiniSearch?: boolean
  @Input() hideMiniCart?: boolean

  @Input() showCart?: boolean
  @Input() showSearch?: boolean
  @Input() showChat?: boolean
  @Input() showPromo?: boolean


  top: any;

  numberOfItems: any
  home: boolean;
  state: any

  isSticky: boolean

  constructor(
    public routerExtSvc: RouterextService,
    public router: Router,
    public checkoutSvc: CheckoutService,
    private ngLocation: Location,
  ){

  }

  checkHome(){
    this.state = this.ngLocation.getState()
    if(this.router.url.includes('/homepage') || this.state.navigationId == 1){
      this.home=true
    }
  }

  ngOnInit() {   
    this.checkHome() 

    this.router.events.subscribe(
      (event)=>{
       if(event instanceof NavigationEnd){
         this.checkHome()
       }
      }
    )

    this.checkoutSvc.cartSubject.subscribe(
      (res)=>{
        if(res){
          //console.log(res)
          this.numberOfItems=res.total_items == null ? 0 :res.total_items
        }
      }
    )

    window.onscroll = ()=>{
      let navbar = document.querySelector("app-top-navigation-bar")
      if(navbar){
        let params=navbar.getBoundingClientRect()
        if (window.pageYOffset > params.height*1.1) {
          this.isSticky=true
        } else {
          this.isSticky=false
        }
      }
    }
    
  }

 
  navigate(){
    if(this.routerLink){
      this.router.navigate([this.routerLink])
    }
    else if(this.state.navigationId == 1){
      this.router.navigate(['/homepage'],{
        replaceUrl: true
      })
    }
    else this.ngLocation.back()
  }

  showDeskhelp(){
    eval("window.fcWidget.open();window.fcWidget.show()")
  }
}
