import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { AppComponent } from './app.component';
import { FcmComponent } from './modules/fcm/fcm.component';

import { FeaturedJournalComponent } from './shared/block/featured-journal/featured-journal.component';
import { FeaturedCategoriesComponent } from './shared/block/featured-categories/featured-categories.component';
import { FeaturedBrandComponent } from './shared/block/featured-brand/featured-brand.component';
import { CategoryComponent } from './components/CatalogModule/category/category.component';
import { PromoComponent } from './components/CatalogModule/promo/promo.component';
import { BrandComponent } from './components/CatalogModule/brand/brand.component';
import { SaleComponent } from './components/CatalogModule/sale/sale.component';
import { CampaignComponent } from './components/CatalogModule/campaign/campaign.component';
import { CampaignViewComponent } from './components/CatalogModule/campaign/campaign-view/campaign-view.component';
import { HomepageComponent } from './components/CatalogModule/homepage/homepage.component';
import { AppRoutingModule } from './app.routing.module';
import { PromoViewComponent } from './components/CatalogModule/promo/promo-view/promo-view.component';
import { SaleViewComponent } from './components/CatalogModule/sale/sale-view/sale-view.component';
import { BrandViewComponent } from './components/CatalogModule/brand/brand-view/brand-view.component';
import { CategoryViewComponent } from './components/CatalogModule/category/category-view/category-view.component';
import { FeaturedCampaignComponent } from './shared/block/featured-campaign/featured-campaign.component';
import { FeaturedSaleComponent } from './shared/block/featured-sale/featured-sale.component';
import { SearchComponent } from './components/CatalogModule/search/search.component';
import { FeaturedProductComponent } from './shared/block/featured-product/featured-product.component';
import { SharedModule } from './shared/shared.module';
import { HelpComponent } from './components/CustomerModule/help/help.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {CookieService} from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    FcmComponent,
    FeaturedJournalComponent,
    FeaturedCategoriesComponent,
    FeaturedBrandComponent,
    CategoryComponent,
    PromoComponent,
    BrandComponent,
    SaleComponent,
    CampaignComponent,
    CampaignViewComponent,
    HomepageComponent,
    PromoViewComponent,
    SaleViewComponent,
    BrandViewComponent,
    CategoryViewComponent,
    FeaturedCampaignComponent,
    FeaturedSaleComponent,
    SearchComponent,
    FeaturedProductComponent,
    HelpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    CookieService
  ],
  bootstrap: [AppComponent],
})

export class AppModule{
  constructor(){}
}
