import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomepageComponent } from './components/CatalogModule/homepage/homepage.component';
import { CampaignComponent } from './components/CatalogModule/campaign/campaign.component';
import { CampaignViewComponent } from './components/CatalogModule/campaign/campaign-view/campaign-view.component';
import { CategoryComponent } from './components/CatalogModule/category/category.component';
import { PromoComponent } from './components/CatalogModule/promo/promo.component';
import { PromoViewComponent } from './components/CatalogModule/promo/promo-view/promo-view.component';
import { SaleComponent } from './components/CatalogModule/sale/sale.component';
import { SaleViewComponent } from './components/CatalogModule/sale/sale-view/sale-view.component';
import { BrandComponent } from './components/CatalogModule/brand/brand.component';
import { BrandViewComponent } from './components/CatalogModule/brand/brand-view/brand-view.component';
import { CategoryViewComponent } from './components/CatalogModule/category/category-view/category-view.component';
import { SearchComponent } from './components/CatalogModule/search/search.component';

const routerModules: Routes=[
    //ORDER MODULE
    {
        path: 'order',
        loadChildren: () => import('./components/OrderModule/order.module').then(m => m.OrderModule),
    },

    //CUSTOMER MODULE
    {
        path: 'customer',
        loadChildren: () => import('./components/CustomerModule/customer.module').then(m => m.CustomerModule),
    },
    //CHECKOUT MODULE
    {
        path: 'secure', 
        loadChildren: () => import('./components/CheckoutModule/checkout.module').then(m => m.CheckoutModule),
    },
    //PRODUCT MODULE
    {
        path: 'product', 
        loadChildren: () => import('./components/ProductModule/product.module').then(m => m.ProductModule),
    },
    //REVIEW MODULE
    {
        path: 'review',
        loadChildren: () => import('./components/ReviewModule/review.module').then(m => m.ReviewModule),
    },
]

const routes: Routes=[
    ...routerModules,
    
    //CATALOG COMPONENT
    {
        path: 'homepage', component:  HomepageComponent
    },
    {
        path: 'campaign', children:[
            {
                path: '', component: CampaignComponent
            },{
                path: '**', component: CampaignViewComponent,
            },
        ],
    },
    {
        path: 'category', children:[
            {
                path:'', component: CategoryComponent,
            },
            {
                path:'**', component: CategoryViewComponent,
            }
        ]
    },
    {
        path: 'promo', children:[
            {
                path: '', component: PromoComponent
            },
            {
                path: '**', component: PromoViewComponent
            }
        ]
    },
    {
        path: 'sale', children:[
            {
                path: '', component: SaleComponent
            },
            {
                path: '**', component: SaleViewComponent
            }
        ]
    },
    {
        path: 'brand', children:[
            {
                path: '', component: BrandComponent
            },
            {
                path: '**', component: BrandViewComponent
            }
        ]
    },
    {
        path: 'search', children:[
            {
                path: '', component: SearchComponent
            },{
                path: '**', component: SearchComponent,
            },
        ],
    },
    {
        path: '**', redirectTo: 'homepage', pathMatch: 'full'
    }
]

@NgModule({
    imports: [ RouterModule.forRoot(routes,{
        scrollPositionRestoration: 'enabled',
    }) ],
    exports: [ RouterModule ]
})

export class AppRoutingModule {}