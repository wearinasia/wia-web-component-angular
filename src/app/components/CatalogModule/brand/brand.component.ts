import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import {Brand} from 'src/app/model/brand'
import { MetaService } from 'src/app/services/meta/meta.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {
  brands: Brand[]
  title = 'Brand'
  isLoading: boolean;
  constructor(
    private catalogSvc: CatalogService,
    public metaSvc: MetaService
  ) { }

  ngOnInit() {
    this.isLoading = true
    this.metaSvc.setTitle("Brand Indonesia")
    this.metaSvc.setTags([
      {
        name: "description",
        content: `Dapatkan koleksi lengkap Brand hanya di Wearinasia. Brand Authorized Online Retailer Indonesia`
      },
    ])

    this.getBrands()
  }

  async getBrands(){
    try{
      this.brands = await this.catalogSvc.getBrandList()
    }catch(e){
      console.warn(e)
    }
    finally{
      this.isLoading=false
    }
  }

  getPicture(i){
    let str = this.brands[i].featured_image.image
    if(str) return str
    else return 'assets/placeholders/wia.jpg'
  }

}
