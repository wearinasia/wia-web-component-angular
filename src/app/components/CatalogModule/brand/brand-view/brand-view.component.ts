import { Component, OnInit, OnDestroy } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { ActivatedRoute, UrlSerializer, Router, NavigationEnd, NavigationStart } from '@angular/router';
import { Brand } from 'src/app/model/brand';
import { faTruckLoading } from '@fortawesome/free-solid-svg-icons';
import { Subscription } from 'rxjs';
import { MetaService } from 'src/app/services/meta/meta.service';

@Component({
  selector: 'app-brand-view',
  templateUrl: './brand-view.component.html',
  styleUrls: ['./brand-view.component.css']
})
export class BrandViewComponent implements OnInit,OnDestroy {  
  faLoad = faTruckLoading

  brand: any

  isLoading: Boolean
  isLazyLoading: Boolean
  isMaximumOfList: Boolean
  currentPage: any

  private query
  private param
  title: string;

  routerSubscription: Subscription

  constructor(
    private activatedRoute: ActivatedRoute,
    public catalogSvc: CatalogService,
    private router: Router,
    private serializer: UrlSerializer,
    private metaSvc: MetaService
  ) {
  }

  ngOnDestroy(){
    if(this.routerSubscription) this.routerSubscription.unsubscribe()
  }

  async ngOnInit() {
    await this.getBrandViewByUrl()
    this.routerSubscription = this.router.events.subscribe(
      (res)=>{
        if(res instanceof NavigationEnd){
          this.getBrandViewByUrl()
        }
      }
    )
  }

  async getBrandViewByUrl(){ 
    this.query=location.pathname
    this.param = this.serializer.serialize(this.router.createUrlTree([''], { queryParams: this.activatedRoute.snapshot.queryParams}))

    try{
      this.isLazyLoading = false
      this.isMaximumOfList = false
      this.isLoading=true

      this.brand= await this.catalogSvc.getBrandView(this.query)
      this.title = this.brand.name
      
      this.metaSvc.setTitle("Brand Indonesia")
      this.metaSvc.setTags([
        {
          name: "description",
          content: this.brand.short_description
        },
      ])
    }catch(e){

    }finally{
      this.isLoading=false
    }
    
  }

  ///forlazyloading
  getCurrentItemLength(){
    return this.brand.products.items.length
  }

  getItemMaxLength(){
    return this.brand.products.count
  }

  async onSuccessRetrieve(data){
    this.brand.products.items=this.brand.products.items.concat(data.products.items)
  }

  onSelectChip(str){
    this.router.navigateByUrl('/'+str)
  }

}
