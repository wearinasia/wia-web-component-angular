import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { ToastService } from 'src/app/services/global/toast.service';
import { Router } from '@angular/router';
import { MetaService } from 'src/app/services/meta/meta.service';
import {highlight} from '../../../const/highlights';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
  featuredProductKey

  modals={
    editorsPick: {
      state: false,
      data: null
    },
    details: {
      state: false,
      data: null
    }
  }

  constructor(
    public catalogSvc: CatalogService,
    public toastService: ToastService,
    public router: Router,
    public metaSvc: MetaService
  ) {
    this.featuredProductKey= highlight.brand
  }

  ngOnInit() {
    this.metaSvc.setTitle("Home")
    this.metaSvc.setTags([
      {
        name: "description",
        content: `Wearinasia adalah omni channel commerce yang fokus pada produk - produk traveling, outdoor dan adventurous livestyle.
        Wearinasia memiliki visi untuk menginspirasi dan menjadi pusat bagi orang-orang aktif dan memiliki jiwa petualang.`
      },
    ])
  }

}
