import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { Category } from 'src/app/model/category';
import { Title, Meta } from '@angular/platform-browser';
import { MetaService } from 'src/app/services/meta/meta.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categories: Category[]
  isLoading: boolean
  title = 'Category'

  constructor(
    private catalogSvc: CatalogService,
    private metaSvc: MetaService
  ) {}

  ngOnInit() {
    this.metaSvc.setTitle("Explore Categories")
    this.metaSvc.setTags([
      {
        name: "description",
        content:"Explore Categories"
      },
      {
        name: "keywords",
        content:"Wearinasia",
      },
      {
        name: "robots",
        content: "INDEX,FOLLOW"
      }
    ])
    
    this.getCategories()
  }

  async getCategories(){
    try{
      this.isLoading=true
      this.categories = await this.catalogSvc.getCategoryList()
     
      //console.log(this.categories)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

}
