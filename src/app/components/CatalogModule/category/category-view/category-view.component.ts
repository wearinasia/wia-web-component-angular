import { Component, OnInit, OnChanges, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { ActivatedRoute, Router, NavigationEnd, UrlSerializer } from '@angular/router';
import { Category } from 'src/app/model/category';

import { faTruckLoading } from '@fortawesome/free-solid-svg-icons'
import { MetaService } from 'src/app/services/meta/meta.service';
import { Subscription } from 'rxjs';
import { highlight } from 'src/app/const/highlights';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.css']
})
export class CategoryViewComponent implements OnInit,OnDestroy{
  title: string;

  faLoad = faTruckLoading

  category: Category

  isLoading: boolean
  isLoadingFilters: boolean

  isLazyLoading: boolean

  private query
  private param

  ids: string

  name: string;
  type: string;
  image: any;

  filters: any

  filtersSelectionCSV: any

  modal={
    filterBy: false
  }

  routerSubscription: Subscription

  constructor(
    public catalogSvc: CatalogService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private serializer: UrlSerializer,
    private metaSvc: MetaService
  ) { }

  ngOnDestroy(){
    if(this.routerSubscription) this.routerSubscription.unsubscribe()
  }

  async ngOnInit() {
    this.init()
    this.routerSubscription=this.router.events.subscribe(
      (res)=>{
        if(res instanceof NavigationEnd){
          this.init(true)
        }
      }
    )
  }

  async init(silent?: boolean){
    await this.getCategoryView(silent)
    await this.getFilters(silent)
  }

  hasChildCategory(){
    try{
      return !this.isLoadingFilters && this.filters.child_category
    }catch(e){
      return false
    }
  }

  async getFilters(silent?: boolean){    
    try{
      if(!silent) this.isLoadingFilters=true
      this.filters = await this.catalogSvc.getFilterCategoryByID(this.category.entity_id)
    }catch(e){

    }finally{
      this.isLoadingFilters=false
    }
  }


  async getCategoryView(silent?: boolean){        
    this.query=location.pathname
    
    this.param = this.serializer.serialize(this.router.createUrlTree([''], { queryParams: this.activatedRoute.snapshot.queryParams}))
    
    try{
      if(!silent) this.isLoading=true

      let res = await this.catalogSvc.getCategoryView(this.query,this.param)
      this.category=res

      this.name=this.category.name
      this.title=this.category.title
      this.type=this.category.type

      this.image=res.featured_image.image_2x

      this.assignFeaturedBrandIds(res)

      if(res.type=='editors_pick'){
        this.name= "Editor's Pick"
      }


      this.metaSvc.setTitle(this.category.name)
      this.metaSvc.setTags([{
        name:"description",
        content: this.category.description
      }])
    
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

  hasFilters(){
    try{
      return !this.isLoadingFilters && this.filters.child_category
    }catch(e){
      return false
    }
  }

  onSuccessRetrieve(res: any){
    if(res){
      this.category.products.items = this.category.products.items.concat(res.products.items)
    }
  }

  onSelectChip(str){
    this.router.navigateByUrl('/'+str)
  }

  filterChanges(queryCSV){
    this.filtersSelectionCSV=queryCSV
  }
  
  onLoading(event){
    this.isLazyLoading=event
  }

  assignFeaturedBrandIds(res){
    this.ids = highlight[res.entity_id]
  }

  isNotEditorsPick(){
    return this.type!=='editors_pick' && this.ids
  }
}