import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Campaign } from 'src/app/model/campaign' 

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {
  campaignList: Campaign[]
  isLoading: boolean
  title = 'Shopping That Matters'

  constructor(
    private catalogSvc: CatalogService,
  ) {

  }

  ngOnInit() {
    this.getCampaigns()
  }

  async getCampaigns(){
    try{
      this.isLoading=true
      this.campaignList = await this.catalogSvc.getCampaigns()
      //console.log(this.campaignList)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

}
