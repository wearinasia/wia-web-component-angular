import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Campaign } from 'src/app/model/campaign';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-campaign-view',
  templateUrl: './campaign-view.component.html',
  styleUrls: ['./campaign-view.component.css']
})
export class CampaignViewComponent implements OnInit {
  
  isLoading: boolean;
  campaign: any;
  description: any
  
  constructor(
    private catalogSvc: CatalogService,
    private activatedRoute: ActivatedRoute,
    private sanitizer: DomSanitizer,
  ) {
    
   
  }

  async ngOnInit() {  
    this.isLoading=true 
    await this.getCampaignViewByUrl()
  }

  async getCampaignViewByUrl(){
    let q = location.pathname
    try{
      this.campaign = await this.catalogSvc.getCampaignViewByUrlParam(q)
      this.description= this.sanitizer.bypassSecurityTrustHtml(this.campaign.description)
      console.log(this.campaign)
    }catch(e){
      console.warn(e)
    }
    finally{
      this.isLoading=false
    }
  }

}
