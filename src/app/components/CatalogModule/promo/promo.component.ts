import { Component, OnInit, OnDestroy } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { Promo } from 'src/app/model/promo';
import { MetaService } from 'src/app/services/meta/meta.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.css']
})
export class PromoComponent implements OnInit, OnDestroy{
  isLoading: Boolean
  title = 'Promos'
  promos: Promo[]

  routerSubs: Subscription

  promoTypes=[
    {
      name: "Voucher",
      value: 'voucher'
    },
    {
      name: "Partnership",
      value: 'partnership'
    },
    {
      name: "Sale",
      value: 'sale'
    }
  ]

  constructor(
    private catalogSvc: CatalogService,
    private metaSvc: MetaService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }
  
  ngOnDestroy(){
    if(this.routerSubs) this.routerSubs.unsubscribe()
  }

  ngOnInit() {
    this.getPromos()
    this.routerSubs=this.router.events.subscribe((s)=>{
      if(s instanceof NavigationEnd){
        this.getPromos()
      }
    })
  }

  async getPromos(){
    let selected = this.activatedRoute.snapshot.queryParams['type']
    try{
      this.isLoading=true

      console.log(selected)

      if(selected == "partnership" || selected == "sale"){
        this.promos = await this.catalogSvc.getPromosOtherThanVouchers(selected)
      }
      else{
        this.promos= await this.catalogSvc.getAllVouchers()
      }
      /*
      else{
        let vouchers = await this.catalogSvc.getAllVouchers()
        let internals = await this.catalogSvc.getPromosOtherThanVouchers("internal")
        let partnerships = await this.catalogSvc.getPromosOtherThanVouchers("partnership")
        let sales = await this.catalogSvc.getPromosOtherThanVouchers("sale")

        this.promos= [
          ...vouchers,
          ...internals,
          ...partnerships,
          ...sales
        ]
      }
      */
      console.log(this.promos)
      this.metaSvc.setTags([
        {
          name: "description",
          content: "Dapatkan promo terkini dari Wearinasia"
        },
      ])
      
    }catch(e){

    }finally{
      this.isLoading=false
    }
  }


  promoClass(item){
    let selected = this.activatedRoute.snapshot.queryParams['type']
    if(selected){
      return {
        'active': item.value == this.activatedRoute.snapshot.queryParams['type']
      }
    }else{
      return {
        'active': item.value == 'voucher'
      }
    }
  }

}
