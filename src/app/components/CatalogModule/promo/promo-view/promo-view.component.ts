import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-promo-view',
  templateUrl: './promo-view.component.html',
  styleUrls: ['./promo-view.component.css']
})
export class PromoViewComponent implements OnInit {
  title: string

  isLoading: boolean

  detail: any

  constructor(
    private catalogSvc: CatalogService,
  ) { }

  ngOnInit() {
    this.title="Promo Details"
    this.getPromoDetails()
  }

  async getPromoDetails(){
    this.isLoading=true
    try{
      this.detail = await this.catalogSvc.getPromoView(location.pathname)
      console.log(this.detail)
    }catch(e){
      console.warn(e)
    }
    finally{
      this.isLoading=false
    }
  }

}
