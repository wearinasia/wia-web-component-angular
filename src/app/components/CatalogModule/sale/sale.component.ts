import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { Sale } from 'src/app/model/sale';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {
  sales: Sale[]
  isLoading: Boolean
  title = 'Sale'

  constructor(
    private catalogSvc: CatalogService
  ) {}

  ngOnInit() {
    this.isLoading=true
    this.getSale()
  }

  async getSale(){
    try{
      this.sales = await this.catalogSvc.getSaleList()
      console.log(this.sales)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

}
