import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, UrlSerializer } from '@angular/router';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { Sale } from 'src/app/model/sale';
import { faTruckLoading } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-sale-view',
  templateUrl: './sale-view.component.html',
  styleUrls: ['./sale-view.component.css']
})
export class SaleViewComponent implements OnInit {
  faLoad = faTruckLoading
  
  sale: Sale


  isLoading: Boolean
  isLazyLoading: Boolean
  isMaximumOfList: Boolean
  currentPage: any

  observer: IntersectionObserver
  
  private query
  private param
  title: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    public catalogSvc: CatalogService,
    private router: Router,
    private serializer: UrlSerializer
  ) {
    
  }

  async ngOnInit() {
    await this.getSaleViewByURL()
  }

  async getSaleViewByURL(){
    this.query=location.pathname
    this.param = this.serializer.serialize(this.router.createUrlTree([''], { queryParams: this.activatedRoute.snapshot.queryParams}))

    try{
      this.isLazyLoading = false
      this.isMaximumOfList = false
      this.isLoading=true

      this.sale = await this.catalogSvc.getSaleView(this.query,this.param)
      this.title = this.sale.title
      //console.log(this.sale)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
      this.registerLazyLoading()
    }
  }

  registerLazyLoading(){
    this.currentPage = 1
    let threshold = [0.25,0.50,0.75,1.00]
    let triggerEl = document.getElementById("scrollArea")

    //do not create lazy loading if the trigger element is not exist
    if(triggerEl == null) return
    
    let options = {
      threshold: threshold,
      rootMargin: '0px 0px 2px 0px',
    }

    this.observer = new IntersectionObserver(entries => {
      entries.forEach(async entry => {
        if (entry.intersectionRatio >= 0.75 && !this.isLazyLoading && !this.isMaximumOfList) {
          try{
            this.currentPage+=1
            this.param=`p=${this.currentPage}`

            this.isLazyLoading = true
            let res: Sale = await this.catalogSvc.getSaleView(this.query,this.param)

            ///concating list of products in the current Sale Category
            this.sale.products.items = this.sale.products.items.concat(res.products.items)

            console.log(this.sale.products.items.length)
            console.log(this.sale.products.count)
            if(this.sale.products.items.length >= this.sale.products.count) this.isMaximumOfList=true
          }catch(e){
            console.log(e)
          }finally{
            this.isLazyLoading =false
          }

        }
      });
    },options);
    
    this.observer.observe(triggerEl);
  }

  async onSuccessRetrieve(data){
    this.sale.products.items=this.sale.products.items.concat(data.products.items)
  }

}
