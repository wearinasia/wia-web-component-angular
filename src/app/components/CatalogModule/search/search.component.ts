import { Component, OnInit } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { Location } from '@angular/common';

import { faSearch,faArrowLeft,faTimes } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  faArrowLeft = faArrowLeft
  faSearch=faSearch
  faTimes = faTimes
  title = 'Search'
  backButton: string 

  searchResult: any

  searchKey: any

  timeout: any

  isSearching: boolean
  referrer: string;
  autofocus: boolean;

  page: any

  constructor(
    public catalogSvc: CatalogService,
    private ngLocation: Location
   
  ) { }

  Brands = [
    {
      title : 'Kinto',
     
    },
    {
      title : 'Fjallraven',
     
    },
    {
      title : 'CHPO',
     
    },
    {
      title : 'DJI',
      
    },
    {
      title : 'Garmin',
     
    },
    {
      title : 'Kiowa',
     
    },
    
    
  ]

  ngOnInit() {
    this.autofocus=true
    this.searchKey = ""
  }

  select(str){
    this.searchKey=str
    this.query()
  }

  async query(){
    if(!this.isMinimalSearchKey()) return
    this.page=1

    clearTimeout(this.timeout)
    this.timeout=setTimeout(async ()=>{
      try{
        this.isSearching=true
        let res = await this.catalogSvc.search(this.searchKey, this.page)
        this.searchResult = res
      }catch(e){
        console.warn(e)
      }finally{
        this.isSearching=false
      }
    },500)
  }

  isMinimalSearchKey(){
    return this.searchKey.length >2
  }
  
  noSearchResult(){
    try{
      return this.searchResult.items == null
    }catch(e){
      return true
    }
  }

  isSearchEmpty(){
    return this.searchKey.length == 0
  }

  clearSearch(){
    this.searchKey=""
    this.searchResult=[]
  }

  async onSuccessRetrieve(data){
    this.searchResult.items=this.searchResult.items.concat(data.items)
  }

  additionalParam(){
    return {
      q: this.searchKey
    }
  }

  async loadMore(){
    this.page+=1
    try{
      let res = await this.catalogSvc.search(this.searchKey, this.page)
      this.searchResult.items=[
        ...this.searchResult.items,
        ...res.items
      ]
    }catch(e){

    }
  }

  goBack(){
    this.ngLocation.back()
  }
}
