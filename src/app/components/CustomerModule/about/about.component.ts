import { Component, OnInit } from '@angular/core';
import { faChevronRight ,faCommentAlt,faPhone,faMap } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  title='Business Profile'

  teams=[
   {
    name : 'Albert',
    position:'COO',
    image_path:'/assets/team/albert.jpg'
   },
   {
    name : 'Andrew',
    position:'CMO',
    image_path:'/assets/team/andrew.jpg'
   },
   {
    name : 'James',
    position:'CTO',
    image_path:'/assets/team/james.jpg'
   },
   {
    name : 'Anti',
    position:'Customer Solution',
    image_path:'/assets/team/anti.jpg'
   },
   {
    name : 'Sinta',
    position:'Business Solution',
    image_path:'/assets/team/sinta.jpg'
   },
   {
    name : 'Alessandro',
    position:'Frontend Developer',
    image_path:'/assets/team/ian.jpg'
   },
   {
    name : 'Leon',
    position:'Backend Developer',
    image_path:'/assets/team/leon.jpg'
   },
   {
    name : 'Monic',
    position:'Designer',
    image_path:'/assets/team/monic.jpg'
   },
   {
    name : 'Vivian',
    position:'Content Writer',
    image_path:'/assets/team/vivian.jpg'
   },
   {
    name : 'Ramdhan',
    position:'Marketing',
    image_path:'/assets/team/ramdhan.jpg'
   },
   
   {
    name : 'Cahya',
    position:'Fullfilment',
    image_path:'/assets/team/cahya.jpg'
   }
  ]

  
  constructor() { }
  faCommentAlt=faCommentAlt
  faPhone= faPhone
  faMap=faMap
  faChevronRight=faChevronRight
  ngOnInit() {
  }
  showDeskhelp(){
    eval("window.fcWidget.open();window.fcWidget.show()")
  }

}
