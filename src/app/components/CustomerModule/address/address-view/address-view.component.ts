import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Address } from 'src/app/model/address';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/global/toast.service';
import { formError, validPhoneNumber } from 'src/app/const/formError';
import { faMapMarked } from '@fortawesome/free-solid-svg-icons';
import { RouterextService } from 'src/app/services/routerext/routerext.service';


@Component({
  selector: 'app-address-view',
  templateUrl: './address-view.component.html',
  styleUrls: ['./address-view.component.css']
})
export class AddressViewComponent implements OnInit {
  formErrorMsg = formError
  faMapMarked=faMapMarked
  isLoading: boolean

  address: Address
  addressID: any

  regionList: any[]
  cityList: any[]

  isProcessing: boolean

  title: string

  modal={
    selectPickpoint: false
  }

  addressForm = new FormGroup({
    entity_id: new FormControl(null,),
    customer_id: new FormControl(null,),
    firstname: new FormControl(null,),
    company: new FormControl(null,),
    city: new FormControl(null,{
      validators: [Validators.required]
    }),
    region: new FormControl(null,),
    postcode: new FormControl(null,{
      validators: [Validators.required]
    }),
    country_id: new FormControl("ID",),
    telephone: new FormControl(null,{
      validators: [Validators.required, validPhoneNumber]
    }),
    region_id: new FormControl(null,{
      validators: [Validators.required]
    }),
    street: new FormControl(null,{
      validators: [Validators.required]
    }),
    location: new FormControl(null,{
      validators: [Validators.required]
    }),
    vat_id: new FormControl(null,),
  })
  isEdit: boolean;
  referrer: any;
  isFromCheckout: boolean;


  constructor(
    private customerSvc: CustomerService,
    private activatedR: ActivatedRoute,
    private toastSvc: ToastService,
    private route: ActivatedRoute,
    private router: Router,
    public routerExtSvc: RouterextService,
  ) { }

  selectedRegion: any

  async ngOnInit() {    

    this.referrer = this.route.snapshot.queryParamMap.get('referrer')
    if(this.referrer == 'checkout') {
      this.isFromCheckout = true
    }

    this.addressID = this.activatedR.snapshot.params.id
    this.isProcessing=false

    if(this.addressID == null){
      this.title = "Add New Address"
      this.regionList = await this.customerSvc.getRegions()
    }else{
      this.title= 'Edit Address'
      await this.getAddress()
    }
    
    this.addressForm.controls.region_id.valueChanges.subscribe(
      async (regionID)=>{
        this.selectedRegion = this.regionList.find((item) => item.region_id == regionID)
        this.addressForm.controls.region.setValue(this.selectedRegion.default_name)
        await this.getCities()
        this.addressForm.controls.city.setValue(null)
      })

    this.addressForm.markAsPristine()
  }

  async getAddress(){
    try{
      this.isLoading=true
      this.address = await this.customerSvc.getAddress(this.addressID)
      //console.log(this.address)
      this.regionList = await this.customerSvc.getRegions()
      let regionID = this.address.region_id
      this.getCities(regionID)
      this.addressForm.patchValue(this.address)

      this.selectedRegion = this.regionList.find((item)=> item.region_id == this.addressForm.controls.region_id.value)
      this.addressForm.controls.region.setValue(this.selectedRegion.default_name)
      
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

  async putAddress(){
    try{
      this.toastSvc.sendMessage("Please wait...")
      this.isProcessing=true
      await this.customerSvc.updateAddress(this.address.entity_id, this.addressForm.value)
      if(this.address.is_shipping_address) {
        await this.customerSvc.setAsShippingAddress(this.address.entity_id)
      }

      this.toastSvc.sendMessage("Updated successfully")
    }catch(e){
      console.warn(e)
    }finally{
      this.isProcessing=false
    }
  }

  async addNewAddress(){
    try{
      this.isProcessing=true
      let res = await this.customerSvc.addNewAddress(this.addressForm.value)
      this.toastSvc.sendMessage("New address has been added successfully")
      if(this.isFromCheckout){
        this.selectThisAddress(res.entity_id)
      }

    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isProcessing=false
      
    }
  }

  async selectThisAddress(id: string){
    try{
      let res = await this.customerSvc.setAsShippingAddress(id)
      //console.log(res)
      this.router.navigate(['/secure/checkout'],{
        replaceUrl: true
      })

    }catch(e){
      console.warn(e)
    }
  }

  async getCities(id?){
    this.cityList = await this.customerSvc.getCitiesOf(id == null ? this.addressForm.controls.region_id.value : id)
  }

  isRegionEmpty(){
    return this.addressForm.controls.region_id.value == null
  }

  triggerModal(name){
    this.modal[name]=true
    switch(name){
      case "selectPickpoint":{
        break;
      }
    }
  }

  closeModal(name){
    this.modal[name] = false
  }

  makeAddress(){
    let address = this.addressForm.value
    let sentence = address.location+', '+address.street+', '+address.city+', '
    sentence+=this.selectedRegion.default_name+', '+address.postcode
    return sentence
  }

  usePinpoint(coordinate){
    this.addressForm.controls.vat_id.setValue(`${coordinate.lat},${coordinate.lng}`)
    console.log(this.addressForm.controls.vat_id.value)
    this.closeModal('selectPickpoint')
  }

  removePinpoint(){
    this.addressForm.controls.vat_id.setValue(null)
  }
}
