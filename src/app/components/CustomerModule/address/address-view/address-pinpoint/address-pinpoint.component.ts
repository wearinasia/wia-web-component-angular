import { Component, OnInit, NgZone, Input, Output,EventEmitter } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { ToastService } from 'src/app/services/global/toast.service';


@Component({
  selector: 'app-address-pinpoint',
  templateUrl: './address-pinpoint.component.html',
  styleUrls: ['./address-pinpoint.component.css']
})
export class AddressPinpointComponent implements OnInit {

  mapLatitude: Number
  mapLongitude: Number

  @Input() fullAddress: any

  pinLatitude: Number;
  pinLongitude: Number;

  @Output() onSelectPinpoint = new EventEmitter()

  zoom = 15;
  private geoCoder;

  isValid: boolean

  constructor(
    private ngZone: NgZone,
    private mapsAPILoader: MapsAPILoader,
    private toastSvc: ToastService
  ) {
    //put map and pin to WIA HQ first
    this.mapLatitude =  -6.175387099999986;
    this.mapLongitude = 106.82714748255614;
  }

  ngOnInit() {
    this.isValid = false
    
    
    //get my address!
    this.ngZone.run(()=>{
      this.mapsAPILoader.load().then(() => {   
        this.geoCoder = new google.maps.Geocoder;

        this.geoCoder.geocode({ 'address': this.fullAddress }, (results, status) => {
          if (status === 'OK') {
            if (results[0]) {
              this.ngZone.run(()=>{
                this.mapsAPILoader.load().then(()=>{
                  this.isValid = true
                  this.pinLatitude=results[0].geometry.location.lat()
                  this.pinLongitude=results[0].geometry.location.lng()
      
                  this.mapLatitude=results[0].geometry.location.lat()
                  this.mapLongitude=results[0].geometry.location.lng()
      
                  this.fullAddress = results[0].formatted_address
                })
              })
            } else {
              this.toastSvc.sendMessage("No Result Found")
              window.alert('No results found');
            }
          } else {
            this.toastSvc.sendMessage("An error has occured, please reopen this dialog")
          }
        })
      });
    })
  }

  markerDragEnd(event: MouseEvent) {
    this.pinLatitude = event.coords.lat;
    this.pinLongitude = event.coords.lng;
    this.getAddress();
  }

  getAddress() {
    this.ngZone.run(()=>{
      this.geoCoder.geocode({ 'location': { lat: this.pinLatitude, lng: this.pinLongitude } }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            this.fullAddress = results[0].formatted_address
          } else {
           // window.alert('No results found');
          }
        } else {
          //window.alert('Geocoder failed due to: ' + status)
        }
   
      }); 
    })
  }

  confirmPinpoint(){
    this.onSelectPinpoint.emit({
      lat: this.pinLatitude,
      lng: this.pinLongitude
    })
    this.toastSvc.sendMessage("Pinpoint has been added successfully")
  }
}
