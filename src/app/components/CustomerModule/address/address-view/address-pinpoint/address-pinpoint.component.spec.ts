import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddressPinpointComponent } from './address-pinpoint.component';

describe('AddressPinpointComponent', () => {
  let component: AddressPinpointComponent;
  let fixture: ComponentFixture<AddressPinpointComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddressPinpointComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddressPinpointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
