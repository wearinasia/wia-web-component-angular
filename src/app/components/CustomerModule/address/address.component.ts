import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Address } from 'src/app/model/address';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterextService } from 'src/app/services/routerext/routerext.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  addresses: Address[]
  isLoading: Boolean
  title = 'Address'
  isScrollingDown: boolean

  backButton: string = '/customer/account'

  referrer: string
  isFromCheckout: boolean

  constructor(
    public routerExtSvc: RouterextService,
    private customerSvc: CustomerService,
    private route: ActivatedRoute,
    
    private router: Router,
  ) {

  }

  ngOnInit() {

    var scrollableElement = document.body;
    var clientX, clientY;

    scrollableElement.addEventListener('wheel', (event: any)=>{
      if (event.deltaY) {
        if(event.deltaY>0) this.isScrollingDown=true
        else this.isScrollingDown=false
      }
    });

    scrollableElement.addEventListener('touchstart', (e)=>{
      // Cache the client X/Y coordinates
      clientX = e.touches[0].clientX;
      clientY = e.touches[0].clientY;
    }, false);
    
    scrollableElement.addEventListener('touchend', (e)=>{
      var deltaX, deltaY;
    
      // Compute the change in X and Y coordinates. 
      // The first touch point in the changedTouches
      // list is the touch point that was just removed from the surface.
      deltaX = e.changedTouches[0].clientX - clientX;
      deltaY = e.changedTouches[0].clientY - clientY;
  
      deltaY < 0 ? this.isScrollingDown=true : this.isScrollingDown=false
    },false);

    // console.log("Previous url "+this.routerExtSvc.getPreviousUrl())
    // console.log("Current url "+this.routerExtSvc.getCurrentUrl())

    this.referrer = this.route.snapshot.queryParamMap.get('referrer')

    // this.referrer = this.routerExtSvc.getPreviousUrl()
     if(this.referrer == 'checkout') this.isFromCheckout = true
    this.getAddresses()

  }

  async getAddresses(){
    try{
      this.isLoading=true
      this.addresses = await this.customerSvc.getAddresses()
      console.log(this.addresses)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

  hasAddresses(){
    if(this.addresses){
      if(this.addresses.length == 0) return false
      else return true
    }else{
      return false
    }
  }

  async selectThisAddress(id: string){
    try{
      let res = await this.customerSvc.setAsShippingAddress(id)
      this.router.navigate(['/','secure','checkout'],{
        replaceUrl: true
      })
    }catch(e){
      console.warn(e)
    }
  }
}
