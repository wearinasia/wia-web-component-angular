import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { AccountComponent } from './account/account.component';
import { ProfileComponent } from './account/profile/profile.component';
import { LoginComponent } from './account/login/login.component';
import { CreateComponent } from './account/create/create.component';
import { ChangepasswordComponent } from './account/changepassword/changepassword.component';
import { ForgotpasswordComponent } from './account/forgotpassword/forgotpassword.component';
import { AddressComponent } from './address/address.component';
import { AddressViewComponent } from './address/address-view/address-view.component';
import { HelpComponent } from './help/help.component';
import { AboutComponent } from './about/about.component';

const routes: Routes=[
    {
        path: 'account', children:[
            {
                path: '', component: AccountComponent,
            },
            {
                path: 'profile', component: ProfileComponent,
            },
            {
                path: 'login', component: LoginComponent,
            },
            {
                path: 'create', component: CreateComponent,
            },
            {
                path: 'changepassword', component: ChangepasswordComponent,
            },
            {
                path: 'forgotpassword', component: ForgotpasswordComponent,
            },
            {
                path: '**', redirectTo: '', pathMatch: 'full',
            },
        ],
    },
    {
        path: 'address', children:[
            {
                path: '', component: AddressComponent
            },
            {
                path: 'add', component: AddressViewComponent
            },
            {
                path:'view/id/:id', component: AddressViewComponent
            },
            {
                path: '**', redirectTo: '', pathMatch: 'full'
            }
        ]
    },
    {
        path: 'help', children:[
            {
                path: '', component: HelpComponent,
            },
            {
                path: '**', redirectTo: '', pathMatch: 'full'
            }
        ]
    },

    {
        path: 'about', children:[
            {
                path: '', component: AboutComponent,
            },
            {
                path: '**', redirectTo: '', pathMatch: 'full'
            }
        ]
    },

    {
        path: '**', redirectTo: 'account', pathMatch: 'full'
    }

]

@NgModule({
    imports: [ RouterModule.forChild(routes)],
    exports: [ RouterModule ]
})

export class CustomerRoutingModule {}