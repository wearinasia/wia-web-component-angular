import { NgModule } from '@angular/core';
import { AccountComponent } from './account/account.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProfileComponent } from './account/profile/profile.component';
import { LoginComponent } from './account/login/login.component';
import { ChangepasswordComponent } from './account/changepassword/changepassword.component';
import { ForgotpasswordComponent } from './account/forgotpassword/forgotpassword.component';
import { CreateComponent } from './account/create/create.component';
import { AddressComponent } from './address/address.component';
import { AddressViewComponent } from './address/address-view/address-view.component';
import { AddressPinpointComponent } from './address/address-view/address-pinpoint/address-pinpoint.component';
import { CustomerRoutingModule } from './customer.routing.module';
import { AgmCoreModule } from '@agm/core';
import { AboutComponent } from './about/about.component';


@NgModule({
  declarations: [
    AccountComponent,
    ProfileComponent,
    LoginComponent,
    ChangepasswordComponent,
    ForgotpasswordComponent,
    CreateComponent,
    AddressComponent,
    AddressViewComponent,
    AddressPinpointComponent,
    AboutComponent
  ],
  imports: [
    CustomerRoutingModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAmlnIAu52yOhMzIPFGxSsDaHB9TXmqhQk',
      libraries: ['places']
    }),
  ],
})
export class CustomerModule { }
