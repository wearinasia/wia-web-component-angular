import { Component, OnInit } from '@angular/core';
import { faAddressBook, faKey, faQuestionCircle,faUser, faReceipt } from '@fortawesome/free-solid-svg-icons';
import { Profile } from 'src/app/model/profile';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ToastService } from 'src/app/services/global/toast.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FcmService } from 'src/app/services/fcm/fcm.service';
import { MetaService } from 'src/app/services/meta/meta.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  isLoading: boolean
  isLoggedIn: boolean

  profile: Profile

  title = 'Account'
  listTile = [
    {
      icon : faUser,
      title : 'Profile',
      subtitle : 'title2',
      linkPath : '/customer/account/profile',
    },
    {
      icon : faKey,
      title : 'Change Password',
      subtitle : 'title2',
      linkPath : '/customer/account/changepassword',

    },
    
    {
      icon : faAddressBook,
      title : 'Address',
      subtitle : 'title2',
      linkPath : '/customer/address',

    },
    {
      icon : faQuestionCircle,
      title : 'Help',
      subtitle : 'title2',
      linkPath : '/customer/help',

    }
  ]


  constructor(
    private customerSvc: CustomerService,
    private toastSvc: ToastService,
    private router: Router,
    private fcmSvc:FcmService,
    private metaSvc: MetaService
  ) { }

  ngOnInit() {
    this.metaSvc.setTitle("Account Panel")
    this.metaSvc.setTags([
      {
        name: "description",
        content:"none"
      }
    ])
    this.getProfile()
  }

  async getProfile(){
    try{
      this.isLoading=true
      this.profile = await this.customerSvc.getCustomerProfile()
      //console.log(this.profile)
      this.isLoggedIn=true
    }catch(e){
      console.warn("Can't get profile because "+e)
      this.isLoggedIn=false
    }finally{
      this.isLoading=false
    }
  }
  
  async logout(){
    try{
      this.isLoading=true
      if(await this.customerSvc.logout()){
        this.fcmSvc.resetFCM()
        this.toastSvc.sendMessage("Logout success!")
        this.getProfile()
      }
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.router.navigate(['/customer/account/'])
    }
  }


  promptLogin(){
    this.toastSvc.sendMessage("Please login to continue", "Login",()=>{
      this.router.navigate(['/customer/account/login'])
    },true)


    
  }



}
