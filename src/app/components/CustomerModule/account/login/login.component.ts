import { Component, OnInit, NgZone } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/global/toast.service';
import { Router } from '@angular/router';
import { RouterextService } from 'src/app/services/routerext/routerext.service';
import { formError } from 'src/app/const/formError';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formErrorMsg = formError

  loginForm = new FormGroup({
    email: new FormControl(null,{ 
      validators: [Validators.required, Validators.email]
    }),
    password: new FormControl(null,{ 
      validators: [Validators.required]
    })
  })
  
  isLoading: Boolean
  hasLoggedInFB: Boolean
  isLoggedIn: boolean

  title = 'Login'
  constructor(
    private customerSvc: CustomerService,
    private toastSvc: ToastService,
    private router: Router,
    private routerExtSvc: RouterextService,
    private ngZone: NgZone
  ) { }

  async ngOnInit() {
    await this.getCustomerProfile()
  }
  
  async getCustomerProfile(){
    try{
      this.isLoggedIn=false
      let res = await this.customerSvc.getCustomerProfile()
      //console.log(res)
      this.isLoggedIn=true
    }catch(e){
      console.warn("Can't get customer profile because "+e)
      this.isLoggedIn=false
    }
  }

  async login(){
    if(this.loginForm.valid){
      try{
        this.isLoading=true
        if(await this.customerSvc.login(this.loginForm.value)){
          this.toastSvc.sendMessage("Login success!")
          this.redirect()
        }
      }catch(e){
        this.toastSvc.sendMessage(e)
      }finally{
        this.isLoading=false
      }
    }
  }

  onSuccessFacebookLogin(res){
    if(res) this.redirect()
  }

  redirect(){
    this.ngZone.run(()=>{
      //console.log("Previous url is " + this.routerExtSvc.getPreviousUrl())
      //console.log("Current url is " + this.routerExtSvc.getCurrentUrl())
      this.router.navigate([this.routerExtSvc.getPreviousUrl()],{
        replaceUrl: true
      })
    })
  }
}
