import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { formError } from 'src/app/const/formError';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  formErrorMsg=formError
  isProcessing: boolean

  title = 'Change Password'
  changePassForm= new FormGroup({
    email: new FormControl(null,{
      validators: [Validators.required, Validators.email]
    }),
    password: new FormControl(null,{
      validators: [Validators.required]
    }),
    new_password: new FormControl(null,{
      validators: [Validators.required]
    })
  })
  
  constructor(
    private customerSvc: CustomerService,
    private toastSvc: ToastService
  ) { }

  ngOnInit() {

  }

  async sendResetPassword(){
    if(this.changePassForm.valid){
      try{
        this.isProcessing=true
        let res = await this.customerSvc.changePassword(this.changePassForm.value)
        this.toastSvc.sendMessage(res.message_dialog)
      }catch(e){
        this.toastSvc.sendMessage(e)
      }finally{
        this.isProcessing=false
      }
    }
  }

}
