import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Profile } from 'src/app/model/profile';
import { FormGroup, FormControl } from '@angular/forms';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  profile: Profile
  isLoading: boolean
  isChanging: boolean
  title = 'Edit Profile'

  profileForm = new FormGroup({
    entity_id: new FormControl(),
    email: new FormControl(),
    customer_bio: new FormControl(),
    customer_photo_url: new FormControl(),
    firstname: new FormControl(),
    lastname: new FormControl(),
    default_shipping: new FormControl(),
    website_id: new FormControl(),
  })

  constructor(
    private customerSvc: CustomerService,
    private toastSvc: ToastService
  ) { }

  ngOnInit() {
    this.isChanging=false
    this.getProfile()
  }

  async getProfile(){
    try{
      this.isLoading=true
      this.profile = await this.customerSvc.getCustomerProfile()
      //console.log(this.profile)
      this.profileForm.patchValue(this.profile)
    }catch(e){
      console.warn("Can't get profile because "+e)
    }finally{
      this.isLoading=false
    }
  }

  async updateProfile(){
    try{
      this.isChanging=true
      await this.customerSvc.updateProfile(this.profileForm.value)
    }catch(e){
      console.warn(e)
    }finally{
      this.isChanging=false
      this.toastSvc.sendMessage('Profile Updated!')
    }
  }
}
