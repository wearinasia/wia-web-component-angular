import { Component, OnInit, NgZone } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { formError } from 'src/app/const/formError';
import { RouterextService } from 'src/app/services/routerext/routerext.service';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  title  = 'Create an Account'
  formErrorMsg=formError
  
  registerForm = new FormGroup({
    email: new FormControl(null,{
      validators: [Validators.required, Validators.email]
    }),
    firstname: new FormControl(null,{
      validators: [Validators.required]
    }),
    lastname: new FormControl(null,{
      validators: [Validators.required]
    }),
    password: new FormControl(null,{
      validators: [Validators.required]
    }),
    confirm_password: new FormControl(null,{
      validators: [Validators.required]
    })
  },{
  })
  isLoading: boolean;


  constructor(
    private customerSvc: CustomerService,
    private ngZone: NgZone,
    private routerExtSvc: RouterextService,
    private router: Router,
    private toastSvc: ToastService,
  ) { }

  ngOnInit() {
 
  }

  async createAccount(){
   
    try{
      this.isLoading=true
      let res = await this.customerSvc.register(this.registerForm.value)
      if(res){
        this.router.navigate(['/customer/account'],{
          replaceUrl: true
        })
      }
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isLoading=false
    }
  }

  onSuccessFacebookLogin(res){
    if(res) this.redirect()
  }

  redirect(){
    this.ngZone.run(()=>{
      this.router.navigate([this.routerExtSvc.getPreviousUrl()],{
        replaceUrl: true
      })
    })
  }

}
