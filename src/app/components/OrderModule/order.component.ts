import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { Order } from 'src/app/model/order';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/global/toast.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})

export class OrderComponent implements OnInit {
  orderList:Order[]
  isLoading: Boolean
  
  isLoggedIn: Boolean

  title = 'Order'
  actionButton: string;

  constructor(
    private customerSvc: CustomerService,
    private router: Router,
    private toastSvc: ToastService
  ) { }

  async ngOnInit() {
    try{
      this.isLoading=true
      await this.customerSvc.getCustomerProfile()
      this.isLoggedIn=true
      await this.getOrders()
    }catch(e){
      this.isLoggedIn=false
    }finally{
      this.isLoading=false
    }
  }

  async getOrders(){
    try{
      this.orderList=await this.customerSvc.getOrders()
      console.log(this.orderList)
    }catch(e){
      console.warn(e)
    }
  }

  getActionButton(status){
    if(status=='pending_payment'){
      return 'Menunggu Pembayaran';
    }
    if(status=='pending'){
      return 'Lanjut ke Pembayaran';
    }
    if(status=='canceled'){
      return 'Reorder';
    }
    if(status=='complete'){
      return 'View';
    }
    else{
      return 'View';
    }
   
  }
  

}
