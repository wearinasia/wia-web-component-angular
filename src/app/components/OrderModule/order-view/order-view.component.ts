import { Component, OnInit } from '@angular/core';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { ActivatedRoute, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Order } from 'src/app/model/order';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { Route } from '@angular/compiler/src/core';
import { ToastService } from 'src/app/services/global/toast.service';
import { TagService } from 'src/app/services/tag/tag.service';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.css']
})
export class OrderViewComponent implements OnInit {
  isLoading: Boolean
  isProcessing: Boolean

  order: Order
  title: any

  constructor(
    private customerSvc: CustomerService,
    private checkoutSvc: CheckoutService,
    private activatedR: ActivatedRoute,
    private router: Router,
    private toastSvc: ToastService,
    private tagSvc: TagService
  ) { }

  ngOnInit() {
    this.getOrderDetail()
  }

  async getOrderDetail(){
    try{
      this.isLoading=true
      this.order = await this.customerSvc.getOrderDetail(this.activatedR.snapshot.params.id)
      this.title= '#' + this.order.increment_id
      console.log(this.order)

      //let's check the tag
      if(this.order.status == "pending_payment") this.tagSvc.purchase_unpaid(this.order.items)
      else if(this.order.status == "complete") this.tagSvc.purchase(this.order.items)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

  async retryPayment(){
    try{
      this.isProcessing=true
      let res = await this.checkoutSvc.repayment(this.order.increment_id)
      this.router.navigate(['/','secure','checkout','payment'],
      { queryParams: {
        token: res.payment.token,
        order_id: this.order.entity_id} }
      )
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isProcessing=false
    }
  }

  async reorder(){
    try{
      this.isProcessing=true
      let res = await this.checkoutSvc.reorder(this.order.increment_id)
      this.router.navigate(['/','secure','checkout','payment'],
      { queryParams: {
        token: res.payment.token,
        order_id: this.order.entity_id} }
      )
    }catch(e){
      this.toastSvc.sendMessage(e)
    }finally{
      this.isProcessing=false
    }
  }

}
