import { Routes, RouterModule } from "@angular/router";
import { OrderComponent } from './order.component';
import { OrderViewComponent } from './order-view/order-view.component';
import { NgModule } from '@angular/core';

const routes: Routes=[
    {
        path: '', component: OrderComponent,
    },
    {
        path: 'view/order_id/:id', component: OrderViewComponent
    },
    {
        path: '**', redirectTo:'', pathMatch: 'full'
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes)],
    exports: [ RouterModule ]
})

export class OrderRoutingModule {}