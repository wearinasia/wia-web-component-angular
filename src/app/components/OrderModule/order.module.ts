import { NgModule } from "@angular/core";
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderRoutingModule } from './order.routing.module';
import { OrderComponent } from './order.component';
import { OrderViewComponent } from './order-view/order-view.component';

@NgModule({
    declarations:[
        OrderComponent,
        OrderViewComponent
    ],
    imports:[
        OrderRoutingModule,
        SharedModule,
    ],

})
export class OrderModule{}