import { Component, OnInit, AfterViewChecked, ViewChild, OnDestroy } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import {  Router, NavigationEnd } from '@angular/router';

import { ToastService } from 'src/app/services/global/toast.service';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import {faClock, faChevronDown,faArrowRight ,faTags,faShoppingBag,faPercentage } from '@fortawesome/free-solid-svg-icons';
import { TagService } from 'src/app/services/tag/tag.service';
import { Subscription } from 'rxjs';
import { MetaService } from 'src/app/services/meta/meta.service';
import { DomSanitizer } from '@angular/platform-browser';
import { CustomerService } from 'src/app/services/customer/customer.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit,OnDestroy{
  product: any
  faClock=faClock
  faArrowRight=faArrowRight
  faTags = faTags
  faPercentage=faPercentage
  faShoppingBag=faShoppingBag
  faChevronDown=faChevronDown
  isLoadingProduct: Boolean

  isAddingToCart :Boolean 

  isOkToAddToCart: boolean
  productOption: any={}
  showMiniCart: boolean;
  minicart: any;
  title: string

  flashSaleTimeUpdate: any
  flashSaleTimeLeft: any

  modal={
    installment: false
  }


  fHours: any
  fMinutes: any
  fSeconds: any
  fFracSecond: any

  routerSubscription: Subscription
  constructor(
    private catalogSvc: CatalogService,
    private checkoutSvc: CheckoutService,
    private toastSvc: ToastService,
    private router: Router,
    private tagSvc: TagService,
    private metaSvc: MetaService,
    private sanitizer: DomSanitizer,
    private customerSvc: CustomerService,
  ) {}

  ngOnDestroy(){
    if(this.routerSubscription) this.routerSubscription.unsubscribe()
    if(this.flashSaleTimeUpdate) clearTimeout(this.flashSaleTimeUpdate)
  }
  
  async ngOnInit() {   
    this.init()
    this.routerSubscription=this.router.events.subscribe(
      (res)=>{
        if(res instanceof NavigationEnd){
          window.scroll({top: 0})
          this.init()
        }
      }
    )

    this.flashSaleTimeUpdate = setInterval(
      ()=>{
        this.calculateFlashsaleTimeleft()
      },
      1000
    )
  }

  async init(){
    this.title="Product Details"
    await this.getProductView()
  }

  async getProductView(){    
    this.isLoadingProduct=true
    let q = location.pathname
    try{
      let res = await this.catalogSvc.getProductView(q)
      console.log(res)
      this.product=res

      if(this.product.options){
          this.product.options.forEach((item)=>{
          this.productOption.super_attribute={}
          this.productOption.super_attribute[item.attribute_id] = null
        })
        this.isOkToAddToCart=false
        //console.log(this.productOption) 
      }else{
        this.isOkToAddToCart=true
      }
      
      //declare {key: the json key name?, value: the title of the key}
      let accordionRaw=[
        {key: "overview", value: "Overview"},
        {key: "short_description", value: "Short Description"},
        {key: "description", value: "Full Details"},
        {key: "specification", value: "Technical Specification"},
        {key: "warranty", value: "Warranty"},
        {key: "video", value: "Video"},
      ]

      this.data=[]
      accordionRaw.forEach((item)=>{
        if(this.product.hasOwnProperty(item.key) && this.product[item.key]){
          this.data.push( {
            "title": item.value,
            "content": this.sanitizer.bypassSecurityTrustHtml(this.product[item.key]),
          })
        }
      })

      this.metaSvc.setTags([
        {
          name:'description', content: this.product.name
        }
      ])

      this.metaSvc.setTitle(this.product.name)

    }catch(e){
      console.warn(e)
    }finally{
      this.isLoadingProduct=false
    }
  }

  async addToCart(){
    try{
      this.isAddingToCart=true
      this.minicart = await this.checkoutSvc.addToCart(this.product.entity_id, this.productOption)
      if(this.minicart.status_code == 200){
        this.tagSvc.addToCart(this.product)
      }
      this.showMiniCart=true
    }catch(e){
      console.warn("Add to cart fails")
      this.toastSvc.sendMessage(e)
    }
    finally{
      this.isAddingToCart=false
    
    }
  }


  data: any

  toggleAccordian(event, index) {
      var element = event.target;
      element.classList.toggle("active");
      if(this.data[index].isActive) {
        this.data[index].isActive = false;
      } else {
        this.data[index].isActive = true;
    }    

  }

  changeProductOption(attribute_id,e){
    let value = e.target.value
    this.productOption.super_attribute[attribute_id] = value
    for(let key in this.productOption.super_attribute){
      if(this.productOption.super_attribute[key] == null){
        this.isOkToAddToCart = false
        return;
      }
    }
    this.isOkToAddToCart=true
  }

  isProductDiscounted(){
    let finalPrice = this.product.final_price
    let price = this.product.price
    return finalPrice != price
  }

  generateInstallmentStartFrom(){
    return this.product.final_price / this.product.installment
  }


  triggerModal(name){
    this.modal[name]=true
    switch(name){
      case "installment":{
        break;
      }
    }
  }

  closeModal(name){
    this.modal[name] = false
  }

  isProductOutOfStock(){
    return this.product.is_in_stock == '0'
  }

  isProductPreorder(){
    return this.product.is_presale == "1"
  }

  isNonPromoItem(){
    return this.product.is_promo == "1"
  }

  getBrandURL(){
    if(this.product.brand_url){
      return this.product.brand_url
      /*
      let res = this.product.brand_url
      res = res.replace('https://wia.id','')
      res = res.replace("https://wearinasia.com","")
      return res
      */
    }
  }

  hasFlashSaleTimeLeft(){
    return this.flashSaleTimeLeft != null
  }
  
  calculateFlashsaleTimeleft(){
    let date: Date
    try{
      date = new Date(this.product.flashdeal_in_minutes)
      this.flashSaleTimeLeft=Math.floor((date.getTime() - new Date().getTime())/1000)
      
      //this.flashSaleTimeLeft-=(7200+1200)

      this.fHours = Math.floor(this.flashSaleTimeLeft / 3600).toString()
      this.fMinutes = Math.floor( (this.flashSaleTimeLeft % 3600) /60 ).toString()
      this.fSeconds = (this.flashSaleTimeLeft % 60).toString()

      if(this.flashSaleTimeLeft < 0){
        this.flashSaleTimeLeft = null
        clearInterval(this.flashSaleTimeUpdate)
      }
    }catch(e){
      console.warn("No flashdeal time left for this product!")
      clearInterval(this.flashSaleTimeUpdate)
    }
  }

  createFlashSaleEndsCountdown(){
    let date = new Date(this.product.flashdeal_in_minutes)
    let secondsFrac=Math.floor((date.getTime() - new Date().getTime())%1000)

    return `${this.fHours.toString().padStart(2,'0')}:${this.fMinutes.toString().padStart(2,'0')}:${this.fSeconds.toString().padStart(2,'0')}`
  }

  async notifyMe(){
    try{
      await this.customerSvc.getCustomerProfile()
      console.log("Can notify me")
      try{
        let res = await this.customerSvc.notifyProduct(this.product.entity_id)
        this.toastSvc.sendMessage("Thank you, you will receive email once this product is in stock")
      }catch(e){
        this.toastSvc.sendMessage("Fail to subscribe, please try again")
      }
    }catch(e){
      this.toastSvc.sendMessage("You are not logged in")
    }
  }
  
}
