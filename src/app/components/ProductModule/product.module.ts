import { NgModule } from "@angular/core";
import { SharedModule } from 'src/app/shared/shared.module';
import { ProductInstallmentComponent } from './product-installment/product-installment.component';
import { ProductPromosComponent } from './product-promos/product-promos.component';
import { ProductRelatedCampaignComponent } from './product-related-campaign/product-related-campaign.component';
import { ProductReviewsComponent } from './product-reviews/product-reviews.component';
import { ProductReviewsSummaryComponent } from './product-reviews-summary/product-reviews-summary.component';
import { ProductUpsaleComponent } from './product-upsale/product-upsale.component';
import { ProductUspComponent } from './product-usp/product-usp.component';
import { ProductComponent } from './product.component';
import { ProductRoutingModule } from './product.routing.module';

@NgModule({
    declarations:[
        ProductInstallmentComponent,
        ProductPromosComponent,
        ProductRelatedCampaignComponent,
        ProductReviewsComponent,
        ProductReviewsSummaryComponent,
        ProductUpsaleComponent,
        ProductUspComponent,
        ProductComponent
    ],
    imports:[
        SharedModule,
        ProductRoutingModule
    ],
})
export class ProductModule{}