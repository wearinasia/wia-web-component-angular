import { Component, OnInit, Input, OnChanges } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { Promo } from 'src/app/model/promo';

@Component({
  selector: 'app-product-promos',
  templateUrl: './product-promos.component.html',
  styleUrls: ['./product-promos.component.css']
})
export class ProductPromosComponent implements OnInit, OnChanges {
  @Input() productID: string

  isLoading: boolean

  hasPromo: boolean

  promos: Promo[]

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    this.ngOnInit()
  }

  constructor(
    private catalogSvc: CatalogService
  ) { }

  ngOnInit() {
    this.getPromos()
  }

  async getPromos(){
    try{
      this.isLoading=true
      this.promos=await this.catalogSvc.getPromosByProductID(this.productID)
      //console.log(this.promos)
      this.hasPromo=true
    }catch(e){
      this.promos=[]
    }finally{
      this.isLoading=false
    }
  }
}
