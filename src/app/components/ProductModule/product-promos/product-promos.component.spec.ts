import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductPromosComponent } from './product-promos.component';

describe('ProductPromosComponent', () => {
  let component: ProductPromosComponent;
  let fixture: ComponentFixture<ProductPromosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductPromosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPromosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
