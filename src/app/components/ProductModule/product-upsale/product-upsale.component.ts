import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';

@Component({
  selector: 'app-product-upsale',
  templateUrl: './product-upsale.component.html',
  styleUrls: ['./product-upsale.component.css']
})
export class ProductUpsaleComponent implements OnInit {
  isLoading: boolean
  relatedProducts: any[]

  @Input() productID: string

  constructor(
    private catalogSvc: CatalogService
  ) { }

  ngOnInit() {
    this.getRelatedProducts()
  }

  async getRelatedProducts(){
    try{
      this.isLoading=true
      this.relatedProducts = await this.catalogSvc.getRelatedProducts(this.productID)
      //console.log(this.relatedProducts)
    }catch(e){

    }finally{
      this.isLoading=false
    }
  }

}
