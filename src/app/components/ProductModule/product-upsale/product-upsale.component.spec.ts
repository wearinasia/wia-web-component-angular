import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUpsaleComponent } from './product-upsale.component';

describe('ProductUpsaleComponent', () => {
  let component: ProductUpsaleComponent;
  let fixture: ComponentFixture<ProductUpsaleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUpsaleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUpsaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
