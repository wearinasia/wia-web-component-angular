import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductInstallmentComponent } from './product-installment.component';

describe('ProductInstallmentComponent', () => {
  let component: ProductInstallmentComponent;
  let fixture: ComponentFixture<ProductInstallmentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductInstallmentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductInstallmentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
