import { Component, OnInit, OnChanges, Input } from '@angular/core';

@Component({
  selector: 'app-product-installment',
  templateUrl: './product-installment.component.html',
  styleUrls: ['./product-installment.component.css']
})
export class ProductInstallmentComponent implements OnInit,OnChanges{
  availableInstallment:any []
  
  @Input() maximumInstallment: any
  @Input() product: any
  selectedInstallmentOption: any

  ngOnChanges(): void {
    this.ngOnInit()
  }
  
  constructor() { }

  activeClass(item){
    return {
      'active': item == this.selectedInstallmentOption
    }
  }
  
  ngOnInit() {
    this.availableInstallment = [3,6,12,24].filter((i) => i<= this.maximumInstallment)
    this.selectedInstallmentOption=this.availableInstallment[0]
  }

  selectOption(month){
    this.selectedInstallmentOption=month
  }

  calculateInstallment(){
    return this.product.final_price/this.selectedInstallmentOption
  }
  
}
