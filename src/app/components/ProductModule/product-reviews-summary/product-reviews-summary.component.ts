import { Component, OnInit, Input } from '@angular/core';
import { Reviews } from 'src/app/model/review';
import { ReviewService } from 'src/app/services/review/review.service';

@Component({
  selector: 'app-product-reviews-summary',
  templateUrl: './product-reviews-summary.component.html',
  styleUrls: ['./product-reviews-summary.component.css']
})
export class ProductReviewsSummaryComponent implements OnInit {
  isLoadingReviews: Boolean
  reviews: Reviews

  @Input() productID: string

  constructor(
    private reviewSvc: ReviewService,
  ) { }

  ngOnInit() {
    this.getProductReviews()
  }

  async getProductReviews(){
    try{
      this.isLoadingReviews=true
      let res=await this.reviewSvc.getReviewsByProductID(this.productID)
      this.reviews=res
      //console.log(res)
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoadingReviews=false
    }
  }

}
