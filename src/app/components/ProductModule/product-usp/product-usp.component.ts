import { Component, OnInit } from '@angular/core';
import { faCreditCard,faUndo, faStore, faBoxOpen} from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-product-usp',
  templateUrl: './product-usp.component.html',
  styleUrls: ['./product-usp.component.css']
})
export class ProductUspComponent implements OnInit {
  faBoxOpen=faBoxOpen
  faStore=faStore
  faUndo=faUndo
  faCreditCard=faCreditCard
  constructor() { }

  ngOnInit() {
  }

}
