import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductUspComponent } from './product-usp.component';

describe('ProductUspComponent', () => {
  let component: ProductUspComponent;
  let fixture: ComponentFixture<ProductUspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductUspComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductUspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
