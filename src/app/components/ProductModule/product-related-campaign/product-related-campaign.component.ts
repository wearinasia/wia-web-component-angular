import { Component, OnInit, Input } from '@angular/core';
import { CatalogService } from 'src/app/services/catalog/catalog.service';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-product-related-campaign',
  templateUrl: './product-related-campaign.component.html',
  styleUrls: ['./product-related-campaign.component.css']
})
export class ProductRelatedCampaignComponent implements OnInit {
  @Input() productID: string
  faChevronRight=faChevronRight
  campaigns: any[]

  isLoading: boolean

  constructor(
    private catalogService: CatalogService
  ) { }

  ngOnInit() {
    this.getRelatedCampaign()
  }

  async getRelatedCampaign(){
    try{
      this.isLoading=true
      this.campaigns = await this.catalogService.getProductCampaign(this.productID)
      //console.log(this.campaigns)
    }catch(e){
      
    }finally{
      this.isLoading=false
    }
  }

}
