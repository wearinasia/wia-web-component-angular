import { Component, OnInit } from '@angular/core';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { ToastService } from 'src/app/services/global/toast.service';
import {  Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { FcmService } from 'src/app/services/fcm/fcm.service';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  faEraser=faTimes

  modal={
    addPersonalization:{
      isOpen: false,
      customizationTemplate: null,
      existingCustomization: null,
      itemID: null
    },
  }

  cartData: any
  profileData: any

  canCheckout: boolean
  
  title = 'Cart'
  isLoading: boolean

  constructor(
    private checkoutSvc: CheckoutService,
    private customerSvc: CustomerService,
    private toastSvc: ToastService,
    private router: Router,
    private fcmSvc: FcmService,
    private formBuilder: FormBuilder
  ) {}

  personalizationForm: FormGroup

  ngOnInit() {
    this.getCart()
  }

  hasPersonalization(quote){
    return quote.product_custom_options_list != null
  }
  
  async getCart(silent?: boolean){
    try{
      if(!silent) this.isLoading=true
      this.cartData = await this.checkoutSvc.getCartData()

      if(this.cartData.quote.every((item) => item.has_error == false)) this.canCheckout = true
      else this.canCheckout=false
            
      //console.log(this.cartData)
      
      try{
        this.profileData=await this.customerSvc.getCustomerProfile()
        this.fcmSvc.setFCMToCart(this.cartData.id, this.profileData.entity_id)
      }catch(e){
        this.fcmSvc.setFCMToCart(this.cartData.id)
      }

      }catch(e){
        console.warn(e)
        this.cartData=null
        this.fcmSvc.resetFCM()
        this.toastSvc.sendMessage("Your cart is empty", "Back to Shop",()=>{
          this.router.navigate(['/'],{
            replaceUrl: true
          })
        },true)

      }finally{
        this.isLoading=false
    }
  }

  async modifyQty(id,newQty){
    if(await this.checkoutSvc.updateQty(id,newQty)){
      this.getCart(true)
    }
  }

  openModal(name, input?){
    this.modal[name].isOpen=true
    switch(name){
      case "addPersonalization":{
        this.modal.addPersonalization.customizationTemplate=input.product_custom_options_list
        this.modal.addPersonalization.itemID = input.item_id

        this.modal.addPersonalization.existingCustomization = input.custom_options

        let form ={}
        this.modal.addPersonalization.customizationTemplate.forEach((item)=>{

          let searchObj = this.modal.addPersonalization.existingCustomization ? this.modal.addPersonalization.existingCustomization.find((option) => option.option_id == item.option_id) : {}

          form[item.option_id]=new FormControl(searchObj ? searchObj.value : null, {})
          
        })

        this.personalizationForm = this.formBuilder.group(form)
        break;
      }
    }
  }

  closeModal(name){
    this.modal[name].isOpen=false
  }

  async setProductOption(option_id,opcode?){
    try{
      let personalizationFormVal= this.personalizationForm.value
      let itemID = this.modal.addPersonalization.itemID

      let searchValue = personalizationFormVal[option_id]
      let res

      switch(opcode){
        case 'remove':{
          res = await this.checkoutSvc.personalize(itemID, option_id, "")
          break;
        }
        default:{
          res = await this.checkoutSvc.personalize(itemID, option_id, searchValue)
        }
      }

      //console.log(res)
    }catch(e){
      console.warn(e)
    }finally{
      this.closeModal('addPersonalization')
      this.getCart(true)
    }
  }

  async removeItem(itemID,i){
    this.cartData.quote.splice(i,1)
    if(await this.checkoutSvc.removeItem(itemID)){
      this.getCart(true)
    }
  }

  isCustomizationNotExistsAtFirst(option_id){
    if(this.modal.addPersonalization.existingCustomization){
      if(this.modal.addPersonalization.existingCustomization.find(item => item.option_id == option_id)) return false
      else return true
    }else{
      return true
    }
  }

  async goToCheckout(){
    try{
      this.canCheckout=false
      await this.customerSvc.getCustomerProfile()
      this.router.navigate(['/','secure','checkout'])
    }catch(e){
      console.warn(e)
      //this.toastSvc.sendMessage("You need to login before checking out")
      this.router.navigate(['/','customer','account','login'])
    }
  }
}
