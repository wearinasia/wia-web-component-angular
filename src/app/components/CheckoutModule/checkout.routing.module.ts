import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { VoucherComponent } from './checkout/voucher/voucher.component';
import { VoucherViewComponent } from './checkout/voucher/voucher-view/voucher-view.component';
import { PaymentComponent } from './checkout/payment/payment.component';

const routes: Routes=[
    {
        path: 'cart', component: CartComponent
    },
    {
        path: 'checkout', children:[
            {
                path: '',component: CheckoutComponent
            },
            {
                path: 'vouchers',component: VoucherComponent
            },
            {
                path: 'vouchers/:id',component: VoucherViewComponent
            },
            {
                path: 'payment', component: PaymentComponent
            },
            {
                path: '**', redirectTo: '',pathMatch: 'full'
            }
        ]
    },
    {
        path: '**', redirectTo: 'cart', pathMatch: 'full'
    }
]

@NgModule({
    imports: [ RouterModule.forChild(routes)],
    exports: [ RouterModule ]
})

export class CheckoutRoutingModule {}
