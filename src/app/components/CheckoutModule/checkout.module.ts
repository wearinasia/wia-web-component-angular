import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { CheckoutRoutingModule } from './checkout.routing.module';
import { CartComponent } from './cart/cart.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { VoucherComponent } from './checkout/voucher/voucher.component';
import { VoucherViewComponent } from './checkout/voucher/voucher-view/voucher-view.component';
import { PaymentComponent } from './checkout/payment/payment.component';

@NgModule({
    declarations:[
        CartComponent,
        CheckoutComponent,
        VoucherComponent,
        VoucherViewComponent,
        PaymentComponent,
    ],
    imports:[
        CheckoutRoutingModule,
        SharedModule
    ]
})
export class CheckoutModule{}