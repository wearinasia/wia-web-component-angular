import { Component, OnInit } from '@angular/core';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { ToastService } from 'src/app/services/global/toast.service';
import { Router } from '@angular/router';
import { faTimes } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.css']
})
export class VoucherComponent implements OnInit {
  isLoading: boolean
  vouchers: any[]
  voucherCode: string = ''
  faTimes=faTimes

  title = "Vouchers"
  
  constructor(
    private checkoutSvc :CheckoutService,
    private toastSvc: ToastService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getVoucherList()
  }

  async getVoucherList(){
    try{
      this.isLoading=true
      this.vouchers=await this.checkoutSvc.getVouchers()
      //console.log(this.vouchers)
      if(this.vouchers == null) this.vouchers=[]
    }catch(e){

    }finally{
      this.isLoading = false
    }
  }

  async useThisVoucher(id?:string){
    if(await this.checkoutSvc.setVoucher(id ? id: this.voucherCode)){
      this.toastSvc.sendMessage("Voucher applied successfully")
      this.router.navigate(['/secure/checkout'],{
        replaceUrl: true
      })
    }else{
      this.toastSvc.sendMessage(`Voucher ${id ? id: this.voucherCode} cannot be applied, please try another`)
    }
  }
}
