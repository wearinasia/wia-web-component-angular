import { Component, OnInit } from '@angular/core';
import { faTags,faLock, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { CheckoutService } from 'src/app/services/checkout/checkout.service';
import { ToastService } from 'src/app/services/global/toast.service';
import { Router } from '@angular/router';
import { CustomerService } from 'src/app/services/customer/customer.service';
import { TagService } from 'src/app/services/tag/tag.service';
import { FcmService } from 'src/app/services/fcm/fcm.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';



@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  title = 'Checkout'
  faTags = faTags
  faLock=faLock
  faChevronDown=faChevronDown

  modal={
    selectShippingMethod: false,
    sendAsGift: false,
  }

  checkoutData: any
  profileData: any

  selectedShippingMethod: any = {}
  
  isLoading: Boolean
  okToCheckout: boolean
  isCheckingOut: boolean

  sendAsGiftForm = new FormGroup({
    recepient: new FormControl(null,{
      validators: [Validators.required]
    }),
    message: new FormControl(null,{
      validators: [Validators.required]
    }),
  })
  
  constructor(
    private checkoutSvc: CheckoutService,
    private router: Router,
    private customerSvc: CustomerService,
    private toastSvc: ToastService,
    private tagSvc: TagService,
    private fcmSvc: FcmService
  ) { }

  async ngOnInit() {
    this.getCheckout()
  }
  
  async getCheckout(silent?){
    try{
      if(!silent) this.isLoading=true
      this.checkoutData = await this.checkoutSvc.getCheckoutData()
      console.log(this.checkoutData)

      this.okToCheckout = this.checkoutData.allow_to_checkout
      
      if(this.checkoutData.shipping_list)
      this.selectedShippingMethod = this.checkoutData.shipping_list.filter((item) => item.code === this.checkoutData.selected_shipping_code)[0]

      this.tagSvc.beginCheckout(this.checkoutData.quote)
      //console.log(this.selectedShippingMethod)

      try{
        this.profileData=await this.customerSvc.getCustomerProfile()
        //console.log(this.profileData)
        this.fcmSvc.setFCMToCheckout(this.checkoutData.id, this.profileData.entity_id)
      }catch(e){
        this.fcmSvc.setFCMToCheckout(this.checkoutData.id)
      }


    }catch(e){
      this.fcmSvc.resetFCM()
      this.router.navigate(['/secure/cart'],{
        replaceUrl: true
      })
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }

  async selectThisShippingMethod(code){
    this.closeModal('selectShippingMethod')
    if(await this.checkoutSvc.selectShippingMethod(code)){
      this.getCheckout(true)
     
    }
  }

  triggerModal(name,opt?){
    this.modal[name]=true
    switch(name){
      case "selectShippingMethod":{
        if(!this.checkoutData.shipping_list){
          this.toastSvc.sendMessage("There are no available shipping methods available")
          this.modal[name]=false
        }
        break;
      }
    }
  }

  closeModalWithoutCallback(name){
    this.modal[name] = false
  }

  closeModal(name){
    this.modal[name] = false

    switch (name){
      case 'sendAsGift':{
        this.sendAsGiftForm.setValue({
          recepient: null,
          message: null
        })
        break;
      }
    }
  }

  async continueToPayment(){
    try{
      this.isCheckingOut=true
      this.tagSvc.beginPayment(this.checkoutData.quote)
      let response = await this.checkoutSvc.createOrder(
        this.sendAsGiftForm.value
      );
      this.router.navigate(['/','secure','checkout','payment'],
      { queryParams: {
        token: response.payment.token,
        order_id: response.order_id} }
      )
      this.fcmSvc.resetFCM()
    }catch(e){
      this.toastSvc.sendMessage(e)
      this.isCheckingOut=false
    }
  }

}
