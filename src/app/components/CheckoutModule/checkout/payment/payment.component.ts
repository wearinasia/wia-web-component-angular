import { Component, OnInit, NgZone, Renderer2 } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
declare var snap: any

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  isValid: Boolean
  isLoaded: Boolean

  private token: any
  private order_id: any

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private ngZone: NgZone,
    private renderer: Renderer2
  ) {

  }

  addJsToElement(): HTMLScriptElement {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "https://app.midtrans.com/snap/snap.js"
    script.setAttribute('data-client-key',"VT-client-7eqsG5L3YcO9ZhwH")
    this.renderer.appendChild(document.body, script)
    return script;
  }

  ngOnInit() {
    this.addJsToElement().onload=()=>{
      this.isLoaded=true
      this.token = this.activatedRoute.snapshot.queryParams["token"]
      this.order_id = this.activatedRoute.snapshot.queryParams["order_id"]
  
      if(this.token != null && this.order_id != null){
        this.isValid=true
        this.pay()
      }else{
        this.isValid = false;
      }
    }
  }

  private pay(){
    snap.pay(this.token,{
      onSuccess: (result)=>{
        this.ngZone.run(()=>{
          this.router.navigate(['/order/view/order_id/'+this.order_id],{
            replaceUrl: true
          })
        })
      },
      onPending: (result)=>{
        this.ngZone.run(()=>{
          this.router.navigate(['/order/view/order_id/'+this.order_id],{
            replaceUrl: true
          })
        })
      },
      onError: (result)=>{
        this.ngZone.run(()=>{
          this.router.navigate(['/order/view/order_id/'+this.order_id],{
            replaceUrl: true
          })
        })
      },
      onClose: ()=>{
        this.ngZone.run(()=>{
          this.router.navigate(['/order/view/order_id/'+this.order_id],{
            replaceUrl: true
          })
        })
        console.warn('customer closed the popup without finishing the payment');
      }
    })
  }

}
