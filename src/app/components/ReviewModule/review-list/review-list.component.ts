import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReviewService } from 'src/app/services/review/review.service';
import { Reviews } from 'src/app/model/review';

@Component({
  selector: 'app-review-list',
  templateUrl: './review-list.component.html',
  styleUrls: ['./review-list.component.css']
})
export class ReviewListComponent implements OnInit {
  private productID;

  isLoading: boolean

  reviews: Reviews

  title: string

  constructor(
    private activatedRoute: ActivatedRoute,
    private reviewSvc: ReviewService
  ) { }

  async ngOnInit() {
    this.title="More Reviews"
    this.productID=this.activatedRoute.snapshot.params.id
    await this.loadReviews()
  }

  async loadReviews(){
    try{
      this.isLoading=true
      let res = await this.reviewSvc.getReviewsByProductID(this.productID)
      //console.log(res)
      this.reviews = res
      this.reviews.reviews.forEach((item)=>{
        item.created = new Date(item.created+'+7')
      })
    }catch(e){
      console.warn(e)
    }finally{
      this.isLoading=false
    }
  }
}
