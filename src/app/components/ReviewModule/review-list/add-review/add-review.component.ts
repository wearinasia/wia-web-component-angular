import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ReviewService } from 'src/app/services/review/review.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/global/toast.service';
import { formError, validPhoneNumber, noZeroStarRating } from 'src/app/const/formError';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { Location } from '@angular/common';

@Component({
  selector: 'app-add-review',
  templateUrl: './add-review.component.html',
  styleUrls: ['./add-review.component.css']
})
export class AddReviewComponent implements OnInit {
  comments=[
    '',
    'Horrible',
    'Bad',
    'Satisfactory',
    'Good',
    'Awesome'
  ]

  productID: string
  reviewPercent: any
  faStar = faStar

  formErrorMsg= formError
  
  reviewForm = new FormGroup({
    title: new FormControl('',{
      validators: Validators.required
    }),
    reviews: new FormControl('',{
      validators: Validators.required
    }),
    rating: new FormControl(null,{
      validators: [noZeroStarRating]
    }),
  })

  constructor(
    private activatedRoute: ActivatedRoute,
    private reviewSvc: ReviewService,
    private toastSvc: ToastService,
    private ngLocation: Location,
  ) {
    this.reviewPercent=0
  }

  ngOnInit() {
    this.productID=this.activatedRoute.snapshot.params.id
  }

  assignValue(val){
    this.reviewForm.controls.rating.setValue(val)
    this.reviewPercent = val*20
  }

  sendReview(){
    if(this.reviewForm.valid){
      try{
        if(this.reviewSvc.writeAReview(this.productID, this.reviewForm.value)){
          this.toastSvc.sendMessage("Review has been created successfully")
          this.ngLocation.back()
        }else{
          this.toastSvc.sendMessage("Unable to create review, please try again")
        }
      }catch(e){
        this.toastSvc.sendMessage("Unable to create review, please try again")
      }
    }else{
      this.reviewForm.markAsDirty()
      console.log(this.reviewForm.controls.rating.errors)
    }
  }

}
