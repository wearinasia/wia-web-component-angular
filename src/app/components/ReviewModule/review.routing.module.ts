import { Routes, RouterModule } from "@angular/router";
import { NgModule } from '@angular/core';
import { ReviewListComponent } from './review-list/review-list.component';
import { AddReviewComponent } from './review-list/add-review/add-review.component';

const routes: Routes=[
    {
        path: 'product/:id', component: ReviewListComponent,
    },
    {
        path: 'product/add/:id', component: AddReviewComponent
    },
]

@NgModule({
    imports: [ RouterModule.forChild(routes)],
    exports: [ RouterModule ]
})

export class ReviewRoutingModule {}