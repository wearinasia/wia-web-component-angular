import { NgModule } from "@angular/core";
import { SharedModule } from 'src/app/shared/shared.module';
import { ReviewListComponent } from './review-list/review-list.component';
import { AddReviewComponent } from './review-list/add-review/add-review.component';
import { ReviewRoutingModule } from './review.routing.module';

@NgModule({
    declarations:[
        ReviewListComponent,
        AddReviewComponent
    ],
    imports:[
        ReviewRoutingModule,
        SharedModule,
    ],

})
export class ReviewModule{}