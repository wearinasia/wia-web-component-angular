import { Component, NgZone, Renderer2, OnInit } from '@angular/core';
import { CheckoutService } from './services/checkout/checkout.service';
import { Router, NavigationEnd } from '@angular/router';
import { MetaService } from './services/meta/meta.service';
import { CatalogService } from './services/catalog/catalog.service';
import {CookieService} from 'ngx-cookie-service'
import { FcmService } from './services/fcm/fcm.service';
import { FcmWiaService } from './services/wia-fcm/fcm-wia.service';

declare let ga: Function;
declare let window: any
declare var yall: any

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  isLoading: boolean
  loaded: boolean
  
  deals: any[]

  modal={
    promoPopup:{
      deals: null,
      isOpen: false
    }
  }

  constructor(
    private checkoutSvc: CheckoutService,
    private router:Router,
    private metaSvc: MetaService,
    private renderer: Renderer2,
    private catalogSvc: CatalogService,
    private cookieSvc:CookieService,
    private fcmSvc: FcmService
  ){
    
    yall({
      observeChanges: true,
    })
    
    this.isLoading=true
    window.onload=()=>{
      this.isLoading=false
      setTimeout(()=>{
        this.loaded=true
      },250)
    }

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scroll(0,0);

        /*
        let r = event.urlAfterRedirects
        if(!r.includes('/secure/checkout') && !r.includes('/secure/cart')){
          this.fcmSvc.resetFCM()
        }
        */

        try{
          console.warn("GA event")
          ga('set', 'page', event.urlAfterRedirects);
          ga('send', 'pageview');
          this.metaSvc.checkIndexability()
        }catch(e){
          console.warn("Error goggle tag!")
        }
      }
    });
  }


  loadFreshchat(): HTMLScriptElement {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = "https://wchat.freshchat.com/js/widget.js"
    this.renderer.appendChild(document.body, script)
    return script;
  }

  closeModal(e){
    this.modal[e].isOpen=false
  }

  ngOnInit(){
    this.checkoutSvc.getCartQty()
    if(!this.cookieSvc.get("dismissPromoPopup")) this.getDeals()


    //load freshchat
    this.loadFreshchat().onload=()=>{
      window.fcWidget.init({ 
        "config": {
          cssNames: {
              widget: 'fc_frame',
              open: 'fc_open',
              expanded: 'fc_expanded'
            },
        "headerProperty": {
        "hideChatButton": true,
        
        }
        },
        token: "93e38ccb-2399-48be-b970-9610d6e5e097",
        host: "https://wchat.freshchat.com"});
        // window.fcWidget.open({ name: "Checkout", replyText: "Thank you for the help."  });
        //window.fcWidget.hide()
          window.fcWidget.on("widget:closed", function(resp) {
            window.fcWidget.hide();
          });
    }

  }

  async getDeals(){
    try{
      this.modal.promoPopup.deals = await this.catalogSvc.getSinglePromotions('popup_banner')

      if(this.modal.promoPopup.deals.promotion_status == "true"){
        this.toggleDoNotShowPromo({
          target:{
            checked: true
          }
        })
        this.modal.promoPopup.isOpen=true
      }
    }catch(e){
      console.warn(e)
    }
  }

  toggleDoNotShowPromo(event){
    if(event.target.checked){
      this.cookieSvc.set("dismissPromoPopup","true", new Date(new Date().setDate(new Date().getDate()+1)))
    }else{
      this.cookieSvc.delete("dismissPromoPopup")
    }
  }

  redirectPromo(){
    this.closeModal('promoPopup')
    this.router.navigateByUrl(this.modal.promoPopup.deals.promotion_url)
  }
}
