var concat = require('concat');

const files = [
    './dist/wia-goods-essentials/runtime-es2015.js',
    './dist/wia-goods-essentials/runtime-es5.js',
    './dist/wia-goods-essentials/polyfills-es2015.js',
    './dist/wia-goods-essentials/polyfills-es5.js',
    './dist/wia-goods-essentials/main-es2015.js',
    './dist/wia-goods-essentials/main-es5.js',
    './dist/wia-goods-essentials/scripts.js'
]

concat(files, './implementation/wia-goods-essentials.js')
console.info('Custom elements created successfully!')